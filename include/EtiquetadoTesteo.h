/*
 * ExpertoEtiquetar.h
 *
 *  Created on: 21 feb. 2019
 *      Author: pablo
 */

#ifndef INCLUDE_ETIQUETADOTESTEO_H_
#define INCLUDE_ETIQUETADOTESTEO_H_

#include "System.h"
#include "funciones3d.h"


extern ORB_SLAM2::System *Sistema;

////////////////////////////////////////Interface de Estrategias de Etiquetar Testeo
class IEstrategiaEtiquetarTesteo
{
public:
	virtual bool EtiquetarTesteo(vector<tuple<double, int, double, double> >::iterator obs, Mat imgFrame) = 0;
	virtual void Free() = 0;
};

typedef IEstrategiaEtiquetarTesteo* ( *CreateEstrategiaEtiquetarTesteoFn)(void);

////////////////////////////////////////Implementaciones de IEstrategiaEtiquetarTesteo
class EstrategiaEtiquetarTesteoAutomatico : public IEstrategiaEtiquetarTesteo
{
public:
    bool EtiquetarTesteo(vector<tuple<double, int, double, double> >::iterator obs, Mat imgFrame);
    void Free() { delete this; }

    static IEstrategiaEtiquetarTesteo *  Create() { return new EstrategiaEtiquetarTesteoAutomatico(); }
};

class EstrategiaEtiquetarTesteoManual : public IEstrategiaEtiquetarTesteo
{
public:
    bool EtiquetarTesteo(vector<tuple<double, int, double, double> >::iterator obs, Mat imgFrame);
    void Free() { delete this; }

    static IEstrategiaEtiquetarTesteo *  Create() { return new EstrategiaEtiquetarTesteoManual(); }
};


//////////////////////////////////////// Factoria para crear instancias de IEstrategiaEtiquetado
class FactoriaEstrategiasEtiquetarTesteo
{
private:
	FactoriaEstrategiasEtiquetarTesteo();
	FactoriaEstrategiasEtiquetarTesteo(const FactoriaEstrategiasEtiquetarTesteo &) { }
	FactoriaEstrategiasEtiquetarTesteo &operator=(const FactoriaEstrategiasEtiquetarTesteo &) { return *this; }

    typedef std::map<int,CreateEstrategiaEtiquetarTesteoFn> FactoryMap;
    FactoryMap m_FactoryMap;
public:
    ~FactoriaEstrategiasEtiquetarTesteo() { m_FactoryMap.clear(); }

    static FactoriaEstrategiasEtiquetarTesteo *Get()
    {
        static FactoriaEstrategiasEtiquetarTesteo instance;
        return &instance;
    }

    void Register(const int tipoMetodo, CreateEstrategiaEtiquetarTesteoFn pfnCreate);
    IEstrategiaEtiquetarTesteo *CreateEstrategiaEtiquetarTesteo(const int tipoMetodo);
};

//////////////////////////////////////// Experto cuya responsabilidad es etiquetar los frames de testeo

class ExpertoEtiquetar{
public:
	ExpertoEtiquetar(string rutaObservaciones, vector<string> vstrImageFilenames, vector<double> vTimestamps, string inputFolder);

	//Main function
	void EtiquetarTesteo();

	std::vector<tuple<double, int, double, double> > cargarObservaciones(string observacionesFile);

protected:
	vector<tuple<double, int, double, double> > observaciones;
	vector<string> vstrImageFilenames;
	vector<double> vTimestamps;
	string inputFolder;
};





#endif /* INCLUDE_ETIQUETADOTESTEO_H_ */
