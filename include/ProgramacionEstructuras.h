/*
 * Estructuras.h
 *
 *  Created on: 24 feb. 2019
 *      Author: pablo
 */

#ifndef INCLUDE_PROGRAMACIONESTRUCTURAS_H_
#define INCLUDE_PROGRAMACIONESTRUCTURAS_H_

#include "System.h"
#include "funciones3d.h"


extern ORB_SLAM2::System *Sistema;

class Estructura{
public:
	virtual Mat reproyectar(ORB_SLAM2::KeyFrame* kf, cv::Mat img) = 0;
	virtual Mat reproyectar(ORB_SLAM2::Frame f, cv::Mat img)= 0;
	virtual bool enPantalla(ORB_SLAM2::Frame f) = 0;
	virtual bool enPantalla(ORB_SLAM2::KeyFrame* kf) = 0;
	vector<Point2d> Reproyectar(cv::Mat Tcw, cv::Mat K);
	vector<Point2d> Reproyectar(ORB_SLAM2::KeyFrame* kf);
	vector<Point2d> Reproyectar(ORB_SLAM2::Frame f);
	int getId() const {return id;}
	vector<ORB_SLAM2::MapPoint*> getPuntos() const {return puntos;}
	const string& getDescripcion() const {return descripcion;}
	void setDescripcion(const string& descripcion) {this->descripcion = descripcion;}
	int getVideo() const {return video;}
	void setVideo(int video) {this->video = video;}
	cv::Mat getKf1() const {return kf1;}
	void setKf1(cv::Mat kf1) {this->kf1 = kf1;}
	cv::Mat getKf2() const {return kf2;}
	void setKf2(cv::Mat kf2) {this->kf2 = kf2;}

protected:
	int id;
	vector<ORB_SLAM2::MapPoint*> puntos;
	string descripcion;
	int video;
	cv::Mat kf1;
	cv::Mat kf2;
	ORB_SLAM2::MapPoint* TriangularHotpoint(pair<long unsigned int, cv::Point> kp1, pair<long unsigned int, cv::Point> kp2,vector<ORB_SLAM2::KeyFrame*> allKFs, ORB_SLAM2::Map* mapa);
	cv::Mat SkewSymmetricMatrix(const cv::Mat &v);
	cv::Mat ComputeF12(ORB_SLAM2::KeyFrame *&pKF1, ORB_SLAM2::KeyFrame *&pKF2);
	Point2d reproyectarPunto(ORB_SLAM2::MapPoint* hp, ORB_SLAM2::Frame f);
	Point2d reproyectarPunto(ORB_SLAM2::MapPoint* hp, ORB_SLAM2::KeyFrame* kf);
	Point2d reproyectarPunto(ORB_SLAM2::MapPoint* hp, cv::Mat Tcw, cv::Mat K);

	/** Serialización agregada para guardar y cargar mapas.*/
	friend class boost::serialization::access;
	friend class Serializer;
	template<class Archivo> void serialize(Archivo&, const unsigned int);
	// Fin del agregado para serialización
};

typedef Estructura* ( *CreateEstructuraFn)(void);

class Punto: public Estructura {

public:
	Punto();//este es implementado en serializer
	Punto(int n);//este es utilizado por la factoría, el parámetro no se usa
	Mat reproyectar(ORB_SLAM2::KeyFrame* kf, cv::Mat img);
	Mat reproyectar(ORB_SLAM2::Frame f, cv::Mat img);
	bool enPantalla(ORB_SLAM2::Frame f);
	bool enPantalla(ORB_SLAM2::KeyFrame* kf);
	static Estructura *  Create() {
		return new Punto(1);
	}

protected:
	/** Serialización agregada para guardar y cargar mapas.*/
	friend class boost::serialization::access;
	friend class Serializer;
	template<class Archivo> void serialize(Archivo&, const unsigned int);
	// Fin del agregado para serialización
};

class Linea: public Estructura {

public:
	Linea();//este es implementado en serializer
	Linea(int n);//este es utilizado por la factoría, el parámetro no se usa
	Mat reproyectar(ORB_SLAM2::KeyFrame* kf, cv::Mat img);
	Mat reproyectar(ORB_SLAM2::Frame f, cv::Mat img);
	bool enPantalla(ORB_SLAM2::Frame f);
	bool enPantalla(ORB_SLAM2::KeyFrame* kf);
	static Estructura *  Create() {
		return new Linea(1);
	}

protected:
	/** Serialización agregada para guardar y cargar mapas.*/
	friend class boost::serialization::access;
	friend class Serializer;
	template<class Archivo> void serialize(Archivo&, const unsigned int);
	// Fin del agregado para serialización
};

class FactoriaEstructuras
{
private:
	FactoriaEstructuras();
	FactoriaEstructuras(const FactoriaEstructuras &) { }
	FactoriaEstructuras &operator=(const FactoriaEstructuras &) { return *this; }

    typedef std::map<int,CreateEstructuraFn> FactoryMap;
    FactoryMap m_FactoryMap;
public:
    ~FactoriaEstructuras() { m_FactoryMap.clear(); }

    static FactoriaEstructuras *Get()
    {
        static FactoriaEstructuras instance;
        return &instance;
    }

    void Register(const int tipo, CreateEstructuraFn pfnCreate);
    Estructura *CreateEstructura(const int tipo);
};

class ExpertoEstructuras{
public:
	bool ProgramarEstructura();
	void EliminarEstructura(string mapFile,ORB_SLAM2::System* Sistema );
	void EditarEstructura(string mapFile,ORB_SLAM2::System* Sistema);
	double CalcularEscala();
};




#endif /* INCLUDE_PROGRAMACIONESTRUCTURAS_H_ */
