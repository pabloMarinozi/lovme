/*
 * HotPoint.h
 *
 *  Created on: 8/7/2018
 *      Author: pablo
 */
#include<System.h>
#include "KeyFrame.h"
#include "ProgramacionEstructuras.h"

//Utilidades
ORB_SLAM2::KeyFrame* BuscarKF(long unsigned int kfId, vector<ORB_SLAM2::KeyFrame*> allKFs);
Estructura* BuscarPunto(int id, vector<Estructura*> puntos);
void drawPoint( cv::Mat image, const cv::Point2d punto, cv::Mat& outImage,const cv::Scalar& color, int radius);
double distanciaPuntoPunto(cv::Mat punto1, cv::Mat punto2);
cv::Vec3d encontrarPuntoDistancia(cv::Vec3d director, cv::Vec3d punto, double distancia);

