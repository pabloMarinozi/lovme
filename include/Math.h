/*
 * Math.h
 *
 *  Created on: 5 feb. 2019
 *      Author: pablo
 */

#ifndef INCLUDE_MATH_H_
#define INCLUDE_MATH_H_

#define INF 10000
#include <opencv2/core.hpp>
using namespace std;

double distanciaPuntoPunto(cv::Point2d punto1, cv::Point2d punto2);
double distanciaPuntoSegmento(cv::Point2d punto, cv::Point2d l1, cv::Point2d l2);
double distanciaPuntoPoligono(cv::Point2d p, vector<cv::Point2d> polygon);
bool isInside(cv::Point2d p, vector<cv::Point2d> polygon);



#endif /* INCLUDE_MATH_H_ */
