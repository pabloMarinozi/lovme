#define CERES_FOUND true
#include <opencv2/calib3d.hpp>
#include <opencv2/core.hpp>
#include <iostream>
#include <fstream>
#include <string>
#include <math.h>
using namespace std;
using namespace cv;

vector<vector<double> > distanciaEjePunto(vector<Mat> R, vector<Mat> t, vector<Mat> points3d);
Vec3d prodVect(Vec3d v1, Vec3d v2);
void guardarDistancias (vector<vector<double> > distPorCam);
Mat calcularRectasVision(vector<Mat> R, vector<Mat> t);
Mat calcularPlanosNormales(Mat rectasVision, double distancia);
Vec3d encontrarPuntoDistancia(Vec3d director, Vec3d punto, double distancia);
Vec3d calcularCentroDeMasa(vector<Vec3d> puntos);
vector<Mat> calcularInterseccionesPlanoRectas(Mat rectasVision, Mat planosNormales);
double distanciaPuntoPunto(Vec3d punto1, Vec3d punto2);
void guardarCsv(Mat& archivo, const char* nombreArchivo);
vector<Vec3d> calcularInterseccionesPlanoIndividualRectas(Mat rectasVision, Mat planoNormal);
Mat calcularPlanoNormal(Mat rectaVision, double distancia);
void calcularFuncionErrorPorPlano(int it, Mat rectasVision, double dist, double incremento);
float dot(cv::Mat v1, cv::Mat v2);
