/*
 * pantalla.h
 *
 *  Created on: 3 sep. 2018
 *      Author: pablo
 */

#ifndef INCLUDE_INTERFAZUSUARIO_H_
#define INCLUDE_INTERFAZUSUARIO_H_

#include "System.h"
#include "funciones3d.h"

extern ORB_SLAM2::System *Sistema;
extern bool primerPuntoAceptado;
extern bool eventLeftClick;
extern bool eventRightClick;
extern bool eventDoubleRightClick;
extern bool eventoClickDerecho;
extern bool eventoClickIzquierdo;
extern bool eventoClickMedio;
extern cv::Point pointSelected;
extern vector<long unsigned int> kfIds;
extern int idPunto;
extern int cantHP;
extern int nHP;
extern int hot;
extern int videoId;
extern string str;
extern string opciones;
extern string errorOpcion;
extern string hotOpcion;

void getScreenResolution(int &width, int &height);
void onMouse(int evt, int x, int y, int flags, void* param);
void onMouse2(int evt, int x, int y, int flags, void* param);
bool preguntaSiNo(string preguntaInicial, string mensajeError);
int preguntaOpciones(string preguntaInicial, std::vector<string> opciones);
int solicitaEntero(string pregunta,int min, int max=INT_MAX);
float solicitaReal(string pregunta, float min, float max = FLT_MAX );
double solicitaRealDoblePrecision(string pregunta, double min, double max = DBL_MAX);
long unsigned int ElegirKeyFrame(vector<ORB_SLAM2::KeyFrame*> excluidos, ORB_SLAM2::System* Sistema);
vector<cv::Point> ElegirPuntos(int cant, long unsigned int kf,ORB_SLAM2::System* Sistema, cv::Mat &im);




#endif /* INCLUDE_INTERFAZUSUARIO_H_ */
