/**
* This file is part of ORB-SLAM2.
*
* Copyright (C) 2014-2016 Raúl Mur-Artal <raulmur at unizar dot es> (University of Zaragoza)
* For more information see <https://github.com/raulmur/ORB_SLAM2>
*
* ORB-SLAM2 is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* ORB-SLAM2 is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with ORB-SLAM2. If not, see <http://www.gnu.org/licenses/>.
*/

#include "FrameDrawer.h"
#include "Tracking.h"
#include "System.h"

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>

#include<mutex>

namespace ORB_SLAM2
{

FrameDrawer::FrameDrawer(Map* pMap):mpMap(pMap)
{
    mState=Tracking::SYSTEM_NOT_READY;
    mIm = cv::Mat(480,640,CV_8UC3, cv::Scalar(0,0,0));
    showKeypoint = false;
}

cv::Mat FrameDrawer::DrawFrame()
{
    cv::Mat im;
    vector<cv::KeyPoint> vIniKeys; // Initialization: KeyPoints in reference frame
    vector<int> vMatches; // Initialization: correspondeces with reference keypoints
    vector<cv::KeyPoint> vCurrentKeys; // KeyPoints in current frame
    vector<bool> vbVO, vbMap; // Tracked MapPoints in current frame
    int state; // Tracking state

    //Copy variables within scoped mutex
    {
        unique_lock<mutex> lock(mMutex);
        state=mState;
        if(mState==Tracking::SYSTEM_NOT_READY)
            mState=Tracking::NO_IMAGES_YET;

        mIm.copyTo(im);

        if(mState==Tracking::NOT_INITIALIZED)
        {
            vCurrentKeys = mvCurrentKeys;
            vIniKeys = mvIniKeys;
            vMatches = mvIniMatches;
        }
        else if(mState==Tracking::OK)
        {
            vCurrentKeys = mvCurrentKeys;
            vbVO = mvbVO;
            vbMap = mvbMap;
        }
        else if(mState==Tracking::LOST)
        {
            vCurrentKeys = mvCurrentKeys;
        }
    } // destroy scoped mutex -> release mutex

    if(im.channels()<3) //this should be always true
        cvtColor(im,im,CV_GRAY2BGR);

    //Draw
    if(state==Tracking::NOT_INITIALIZED) //INITIALIZING
    {
        for(unsigned int i=0; i<vMatches.size(); i++)
        {
            if(vMatches[i]>=0)
            {
                cv::line(im,vIniKeys[i].pt,vCurrentKeys[vMatches[i]].pt,
                        cv::Scalar(0,255,0));
            }
        }        
    }
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////DIBUJA LOS MAPPOINTS EN EL FRAME////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    else if(state==Tracking::OK && !mbOnlyTracking) //TRACKING
    {
    	mnTracked=0;
    	mnTrackedVO=0;
    	const float r = 5;
    	const float r2 = 5;
    	const int n = vCurrentKeys.size();
    	bool hayHotpoint = false;
    	if (!showKeypoint) {
    		for(int i=0;i<n;i++)
    		{
    			cv::Point2f pt1,pt2,pt3,pt4;
    			pt1.x=vCurrentKeys[i].pt.x-r;//Para hacer un cuadrado
    			pt1.y=vCurrentKeys[i].pt.y-r;
    			pt2.x=vCurrentKeys[i].pt.x+r;
    			pt2.y=vCurrentKeys[i].pt.y+r;
    			pt3.x=vCurrentKeys[i].pt.x-r2;//Para hacer un cuadrado
    			pt3.y=vCurrentKeys[i].pt.y-r2;
    			pt4.x=vCurrentKeys[i].pt.x+r2;
    			pt4.y=vCurrentKeys[i].pt.y+r2;
    			if(vbVO[i] || vbMap[i])
    			{
    				// This is a match to a MapPoint in the map
    				if(vbMap[i])
    				{
    					if(hotpoints[i] && !hayHotpoint){
    						cv::rectangle(im,pt3,pt4,cv::Scalar(0,0,255));
    						cv::circle(im,vCurrentKeys[i].pt,16,cv::Scalar(255,191,0),3);
    						mnTracked++;
    						//Llamar a metodo abrir interfaz
    						cv::Mat img = cv::imread(iuPath,-1);
    						drawSinterface(img);
    						hayHotpoint = true;

    					}else
    					{
    						//cv::rectangle(im,pt1,pt2,cv::Scalar(0,255,0));
    						cv::circle(im,vCurrentKeys[i].pt,2,cv::Scalar(255,0,0),-1);
    						mnTracked++;
    					}

    				}
    			}
    		}
    		if(!hayHotpoint){
    			desactivarSinterface();
    		}
    	}else{
    		for(int i=0;i<n;i++)
    		{
    			cv::Point2f pt1,pt2,pt3,pt4;
    			pt1.x=vCurrentKeys[i].pt.x-r;//Para hacer un cuadrado
    			pt1.y=vCurrentKeys[i].pt.y-r;
    			pt2.x=vCurrentKeys[i].pt.x+r;
    			pt2.y=vCurrentKeys[i].pt.y+r;
    			pt3.x=vCurrentKeys[i].pt.x-r2;//Para hacer un cuadrado
    			pt3.y=vCurrentKeys[i].pt.y-r2;
    			pt4.x=vCurrentKeys[i].pt.x+r2;
    			pt4.y=vCurrentKeys[i].pt.y+r2;
    			if(vbVO[i] || vbMap[i]){
    				if(vbMap[i])
    				{
    					//cv::rectangle(im,pt1,pt2,cv::Scalar(0,255,0));
    					cv::circle(im,vCurrentKeys[i].pt,2,cv::Scalar(0,255,0),-1);
    					mnTracked++;
    				}
    				else// This is match to a "visual odometry" MapPoint created in the last frame
    				{
    					//cv::rectangle(im,pt1,pt2,cv::Scalar(255,0,0));
    					cv::circle(im,vCurrentKeys[i].pt,2,cv::Scalar(255,0,0),-1);
    					mnTrackedVO++;
    				}
    			}
    		}
    	}
    }

    cv::line(im, cv::Point(im.cols/2-20,im.rows/2),cv::Point(im.cols/2+20,im.rows/2),cv::Scalar(255,255,0), 3);
    cv::line(im, cv::Point(im.cols/2,im.rows/2-20),cv::Point(im.cols/2,im.rows/2+20),cv::Scalar(255,255,0), 3);

    cv::Mat imWithInfo;
    DrawTextInfo(im,state, imWithInfo);


    return imWithInfo;
}


void FrameDrawer::DrawTextInfo(cv::Mat &im, int nState, cv::Mat &imText)
{
	stringstream s;
	if(nState==Tracking::NO_IMAGES_YET)
		s << " WAITING FOR IMAGES";
	else if(nState==Tracking::NOT_INITIALIZED)
		s << " TRYING TO INITIALIZE ";
	else if(nState==Tracking::OK)
	{
		if(!mbOnlyTracking)
			s << "SLAM MODE |  ";
		else
			s << "LOCALIZATION | ";
		int nKFs = mpMap->KeyFramesInMap();
		int nMPs = mpMap->MapPointsInMap();
		s << "KFs: " << nKFs << ", MPs: " << nMPs << ", Matches: " << mnTracked;
		if(mnTrackedVO>0)
			s << ", + VO matches: " << mnTrackedVO;
	}
	else if(nState==Tracking::LOST)
	{
		s << " TRACK LOST. TRYING TO RELOCALIZE ";
	}
	else if(nState==Tracking::SYSTEM_NOT_READY)
	{
		s << " LOADING ORB VOCABULARY. PLEASE WAIT...";
	}

	int baseline=0;
	cv::Size textSize = cv::getTextSize(s.str(),cv::FONT_HERSHEY_PLAIN,1,1,&baseline);

	imText = cv::Mat(im.rows+textSize.height+10,im.cols,im.type());
	im.copyTo(imText.rowRange(0,im.rows).colRange(0,im.cols));
	imText.rowRange(im.rows,imText.rows) = cv::Mat::zeros(textSize.height+10,im.cols,im.type());
	cv::putText(imText,s.str(),cv::Point(5,imText.rows-5),cv::FONT_HERSHEY_PLAIN,1,cv::Scalar(255,255,255),1,8);

}

void FrameDrawer::Update(Tracking *pTracker)
{
	unique_lock<mutex> lock(mMutex);
	if(pTracker->mState==Tracking::OK){
		pTracker->mImColor.copyTo(mIm);
	}else{
		pTracker->mImGray.copyTo(mIm);
	}
	mvCurrentKeys=pTracker->mCurrentFrame.mvKeys;
	N = mvCurrentKeys.size();
	mvbVO = vector<bool>(N,false);
	mvbMap = vector<bool>(N,false);
	hotpoints = vector<bool>(N,false);
	mbOnlyTracking = pTracker->mbOnlyTracking;

	if(pTracker->mLastProcessedState==Tracking::NOT_INITIALIZED)
	{
		mvIniKeys=pTracker->mInitialFrame.mvKeys;
		mvIniMatches=pTracker->mvIniMatches;
	}
	else if(pTracker->mLastProcessedState==Tracking::OK)
	{
		for(int i=0;i<N;i++)
		{
			MapPoint* pMP = pTracker->mCurrentFrame.mvpMapPoints[i];
			if(pMP)
			{
				if(!pTracker->mCurrentFrame.mvbOutlier[i])
				{
					if(pMP->Observations()>0){
						//corresponde a un mappoint
						mvbMap[i]=true;

						if(pMP->isHotpoint){
							if(pMP->isPointed){
								//OBTENEMOS PATH DE INTERFAZ
								//booleano activar interfaz
								//ese mappoint es hotpoint y está siendo apuntado
								iuPath = pTracker->getIU(pMP->mnId);
								hotpoints[i]=true;
							}

							//guardo sus coordenadas 2d para no reproyectar
							cv::Point2d point;
							point.x = mvCurrentKeys[i].pt.x;
							point.y = mvCurrentKeys[i].pt.y;
							//pTracker->savePoint2d(point);
						}


					}
					else
						mvbVO[i]=true;
                }
            }
        }
    }
    mState=static_cast<int>(pTracker->mLastProcessedState);
}

vector<bool> FrameDrawer::GetMvbMap(){
	unique_lock<mutex> lock(mMutex);
	return mvbMap;
}

vector<bool> FrameDrawer::GetMvbV0(){
	unique_lock<mutex> lock(mMutex);
	return mvbVO;
}

vector<cv::KeyPoint> FrameDrawer::GetKeypoints(){
	unique_lock<mutex> lock(mMutex);
	return mvCurrentKeys;
}

void FrameDrawer::showKeypoints(){
	unique_lock<mutex> lock(mMutex);
	showKeypoint = true;
}
void FrameDrawer::hideKeypoints(){
	unique_lock<mutex> lock(mMutex);
	showKeypoint = false;
}

void FrameDrawer::drawSinterface(cv::Mat img){
	sinterface = img;
}

void FrameDrawer::desactivarSinterface(){
	sinterface = cv::imread("apuntar.png",-1);
}

cv::Mat FrameDrawer::getSinterface(){
	return sinterface;
}
/*abrir interfaz
//while(booleano){
	imshow(im leida de path, cvcolor)
*/

} //namespace ORB_SLAM
