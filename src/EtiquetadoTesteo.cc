/*
 * EtiquetadoTesteo.cc
 *
 *  Created on: 21 feb. 2019
 *      Author: pablo
 */

#include "EtiquetadoTesteo.h"
#include "CreadorPunto3D.h"
#include "InterfazUsuario.h"
#include "Math.h"


bool EstrategiaEtiquetarTesteoManual::EtiquetarTesteo(vector<tuple<double, int, double, double> >::iterator obs, Mat imgFrame){
	//Declaración de Variables
	ofstream test;
	cv::Point pointSelected, lastSelected, puntoTest, esquina1,esquina2;
	esquina1.x = puntoTest.x = pointSelected.x = lastSelected.x = -1;
	esquina1.y = puntoTest.y = pointSelected.y = lastSelected.x = -1;
	bool exito = false;
	//resize(imgFrame,imgFrame,cvSize(ancho - (cols*(alto/2)/rows),(imgFrame.rows*(ancho-cols*(alto/2)/rows))/imgFrame.cols));
	cv::Point2d p2d = Point2d(get<2>(*obs),get<3>(*obs));
	cv::Scalar color(0,255,255);
	circle(imgFrame, p2d, 5, color, 2);
	cv::imshow("Frame Testeo", imgFrame);

	//decidir si es posible etiquetar el frame
	vector<string> opciones = {
			"Puedo verlo.",
			"Está obstruido, pero puedo marcar donde debería estar el punto.",
			"El punto se encuentra muy lejos."
	};
	int opcion = preguntaOpciones("\n\n OBSERVE EL FRAME "+to_string(get<0>(*obs))+" Y TRATE DE ENCONTRAR EL PUNTO QUE SE MARCA EN LOS DOS TESTFRAMES",opciones);
	cout<<"Eligió la opcion "<<opcion<<endl;

	//si es perfectamente visible
	if (opcion==1) {
		cout<<"\nHaga click donde se encuentre el punto que se ve los testframes 1 y 2"<<endl;
		for (;;) {
			if (pointSelected.x != lastSelected.x && pointSelected.y != lastSelected.y) {
				cout << "Nuevo punto seleccionado: ("<<pointSelected.x<<","<<pointSelected.y<<")"<< endl;
				Mat img2 = imgFrame.clone();
				circle(img2, pointSelected, 5, cv::Scalar(255,0,0,255), 2);
				cv::imshow("Frame Testeo", img2);
				cv::waitKey(200);
				cout << "Si está de acuerdo con el punto seleccionado, presione el botón derecho. Sino, vuelva a seleccionar otro punto"<< endl;
				lastSelected.x = pointSelected.x;
				lastSelected.y = pointSelected.y;
			}
			if (eventoClickDerecho) {//si presiona click derecho se guarda el punto seleccionado
				if (pointSelected.x == -1) {
					cout<<"\nSE HA CANCELADO LA SELECCIÓN DE PUNTOS SOBRE ESTA IMAGEN. SE GUARDARÁ LA OBSERVACIÓN PARA QUE SEA EVALUADA EN OTRA OCASIÓN."<<endl;
					exito = false;
					break;
				}else{
					cout<<"\nEl punto ingresado ha sido confirmado y se marcará en blanco"<<endl;
					drawPoint(imgFrame,pointSelected,imgFrame,cv::Scalar(255,255,255,255),5);
					cv::imshow("Frame Testeo", imgFrame);
					eventoClickDerecho = false;
					exito = true;
					puntoTest = pointSelected;
					pointSelected.x = lastSelected.x = -1;
					pointSelected.y = lastSelected.x = -1;
					break;
				}
			}
			if (eventoClickMedio){
				cout<<"\nSE HA CANCELADO LA SELECCIÓN DE PUNTOS SOBRE ESTA IMAGEN. SE GUARDARÁ LA OBSERVACIÓN PARA QUE SEA EVALUADA EN OTRA OCASIÓN."<<endl;
				cv::destroyWindow("ORB-SLAM2: KeyFrame Selected");
				eventoClickMedio = false;
				exito = false;
				break;
			}
			sleep(1);
		}
		if (exito) {
			auto end = std::chrono::system_clock::now();
			std::time_t end_time = std::chrono::system_clock::to_time_t(end);
			char *endtime = std::ctime(&end_time);
			endtime[strlen(endtime) - 1] = 0;
			test.open("resultados.txt",fstream::app);
			test << get<1>(*obs) <<","<<get<0>(*obs)<<","<<endtime<<","<<p2d.x<<","<<p2d.y<<","<<"VISIBLE"<<","<<puntoTest.x<<","<<puntoTest.y<<","<<distanciaPuntoPunto(p2d,puntoTest)<<endl;
			test.close();
		}
	}
	//si está obstruido
	else if(opcion == 2){
		vector<cv::Point2d> polygon;
		cout<<"\nSeleccione un área pequeña que encierre el punto que se ve los testframes 1 y 2."<<endl<<" Marque el primer punto con click izquierdo."<<endl;
		for (;;) {
			if (pointSelected.x != lastSelected.x && pointSelected.y != lastSelected.y && !primerPuntoAceptado) {//se marcó el primer punto pero no se ha confirmado
				eventoClickIzquierdo = false;
				cout << "Nuevo punto seleccionado: ("<<pointSelected.x<<","<<pointSelected.y<<")"<< endl;
				Mat img2 = imgFrame.clone();
				circle(img2, pointSelected, 5, cv::Scalar(255,0,0,255), 2);
				cv::imshow("Frame Testeo", img2);
				cv::waitKey(200);
				lastSelected.x = pointSelected.x;
				lastSelected.y = pointSelected.y;
			}
			if (esquina1.x != -1 && esquina1.y != -1){//se está moviendo el mouse con el primer punto confirmado
				Mat img2 = imgFrame.clone();
				rectangle(img2,esquina1,pointSelected,cv::Scalar(255,0,0,255),5);
				cv::imshow("Frame Testeo", img2);
				cv::waitKey(200);
			}
			if (eventoClickIzquierdo && primerPuntoAceptado){//se marcó el segundo punto con el primer punto confirmado
				eventoClickIzquierdo = false;
				Mat img2 = imgFrame.clone();
				cout << "Nuevo punto seleccionado: ("<<pointSelected.x<<","<<pointSelected.y<<")"<< endl;
				esquina2 = pointSelected;
				rectangle(img2,esquina1,esquina2,cv::Scalar(255,255,255,255),5);
				cv::imshow("Frame Testeo", img2);
				cv::waitKey(200);
				int opcion;
				cout<<"\n¿Está satisfecho con el área marcada?"<<endl;
				cout<<"1: SÍ"<<endl;
				cout<<"2: NO"<<endl;
				cin >> opcion;
				// input validation
				while (cin.fail() || opcion >2  || opcion < 1) {
					cin.clear(); // clear input buffer to restore cin to a usable state
					cin.ignore(INT_MAX, '\n'); // ignore last input
					cout<<"\n¿Está satisfecho con el área marcada?"<<endl;
					cout<<"1: SÍ"<<endl;
					cout<<"2: NO"<<endl;
					cin >> opcion;
				}
				if(opcion == 1){//se guarda el área ingresada
					primerPuntoAceptado = false;
					exito = true;
					polygon.push_back(esquina1);
					polygon.push_back(Point2d(esquina2.x,esquina1.y));
					polygon.push_back(esquina2);
					polygon.push_back(Point2d(esquina1.x,esquina2.y));
					cout<<"se ha aceptado el rectángulo con esquinas en:"<<endl;
					for(int i = 0; i<polygon.size();i++){
						cout<<"("<<polygon[i].x<<","<<polygon[i].y<<")"<<endl;
					}
					pointSelected.x = lastSelected.x = -1;
					pointSelected.y = lastSelected.x = -1;
					break;
				}else{//se vuelve a seleccionar el segundo punto
					cout<<"Ahora marque la esquina opuesta o click derecho para cancelar"<<endl;
				}
			}
			if (eventoClickDerecho) {//si presiona click derecho se guarda el punto seleccionado
				if (pointSelected.x == -1) {
					eventoClickDerecho = false;
					cout<<"\nSE HA CANCELADO LA SELECCIÓN DE PUNTOS SOBRE ESTA IMAGEN. SE GUARDARÁ LA OBSERVACIÓN PARA QUE SEA EVALUADA EN OTRA OCASIÓN."<<endl;
					exito = false;
					break;
				}else if(!primerPuntoAceptado){
					primerPuntoAceptado = true;
					cout<<"Ahora marque la esquina opuesta o click derecho para cancelar"<<endl;
					esquina1.x = lastSelected.x = pointSelected.x;
					esquina1.y = lastSelected.y = pointSelected.y;
					eventoClickDerecho = false;
				}else if(primerPuntoAceptado){
					primerPuntoAceptado = false;
					eventoClickDerecho = false;
					cout<<"SE HA CANCELADO EL PRIMER PUNTO";
					cout<<"\nSeleccione un área pequeña que encierre el punto que se ve los testframes 1 y 2."<<endl<<" Marque el primer punto con click izquierdo."<<endl;
					esquina1.x = pointSelected.x = lastSelected.x = -1;
					esquina1.y = pointSelected.y = lastSelected.x = -1;
					cv::imshow("Frame Testeo", imgFrame);
					cv::waitKey(200);
				}
			}
			if (eventoClickMedio){
				cout<<"\nSE HA CANCELADO LA SELECCIÓN DE PUNTOS SOBRE ESTA IMAGEN. SE GUARDARÁ LA OBSERVACIÓN PARA QUE SEA EVALUADA EN OTRA OCASIÓN."<<endl;
				cv::destroyWindow("ORB-SLAM2: KeyFrame Selected");
				eventoClickMedio = false;
				exito = false;
				break;
			}
			//sleep(1);
		}
		if (exito) {
			auto end = std::chrono::system_clock::now();
			std::time_t end_time = std::chrono::system_clock::to_time_t(end);
			char *endtime = std::ctime(&end_time);
			endtime[strlen(endtime) - 1] = 0;
			bool buenaReproyeccion = isInside(p2d,polygon);
			double distancia = distanciaPuntoPoligono(p2d,polygon);
			cout<<distancia<<endl;
			if(buenaReproyeccion) cout<<"la reproyección cae dentro del area indicada!"<<endl;
			string resultado = buenaReproyeccion ? to_string(0) : to_string(distancia);
			test.open("resultados.txt",fstream::app);
			test << get<1>(*obs) <<","<<get<0>(*obs)<<","<<endtime<<","<<p2d.x<<","<<p2d.y<<","<<"OBSTRUIDO"<<","<<esquina1.x<<","<<esquina1.y<<","<<esquina2.x<<","<<esquina2.y<<","<<
					resultado <<"\n";
			test.close();
			esquina1.x = esquina2.x = esquina1.y = esquina2.y = -1;
		}
	}else{//está muy lejos
		cout<<"SE HA MARCADO QUE ESTE PUNTO ESTÁ MUY LEJOS COMO PARA EVALUAR SI SU REPROYECCIÓN ES EXACTA."<<endl;
		auto end = std::chrono::system_clock::now();
		std::time_t end_time = std::chrono::system_clock::to_time_t(end);
		char *endtime = std::ctime(&end_time);
		endtime[strlen(endtime) - 1] = 0;
		test.open("resultados.txt",fstream::app);
		test << get<1>(*obs) <<","<<get<0>(*obs)<<","<<endtime<<","<<p2d.x<<","<<p2d.y<<","<<"INDISTINGUIBLE"<<","<<"-"<<","<<"-"<<","<<"-"<<"\n";
		test.close();
		exito = true;
	}
	return exito;
}

bool EstrategiaEtiquetarTesteoAutomatico::EtiquetarTesteo(vector<tuple<double, int, double, double> >::iterator obs, Mat imgFrame){
	return true;
}

void FactoriaEstrategiasEtiquetarTesteo::Register(const int tipoMetodo, CreateEstrategiaEtiquetarTesteoFn pfnCreate)
{
    m_FactoryMap[tipoMetodo] = pfnCreate;
}

FactoriaEstrategiasEtiquetarTesteo::FactoriaEstrategiasEtiquetarTesteo(){
	Register(1, &EstrategiaEtiquetarTesteoAutomatico::Create);
	Register(2, &EstrategiaEtiquetarTesteoManual::Create);
}

IEstrategiaEtiquetarTesteo *FactoriaEstrategiasEtiquetarTesteo::CreateEstrategiaEtiquetarTesteo(const int tipoMetodo)
{
	FactoryMap::iterator it = m_FactoryMap.find(tipoMetodo);
	if( it != m_FactoryMap.end() )
		return it->second();
	return NULL;
}

std::vector<tuple<double, int, double, double> > ExpertoEtiquetar::cargarObservaciones(string observacionesFile){
	std::vector<tuple<double, int, double, double> > observaciones;
	ifstream f;
	f.open(observacionesFile.c_str());
	while(!f.eof())
	{
		string s;
		getline(f,s);
		if(!s.empty())
		{
			stringstream ss;
			ss << s;
			double t,x,y;
			int punto;
			ss >> t;
			ss >> punto;
			ss >> x;
			ss >> y;
			tuple<double, int, double, double> par = make_tuple(t,punto,x,y);
			observaciones.push_back(par);
		}
	}
	return observaciones;
}

ExpertoEtiquetar::ExpertoEtiquetar(string rutaObservaciones, vector<string> vstrImageFilenames, vector<double> vTimestamps, string inputFolder){
	observaciones = cargarObservaciones(rutaObservaciones);
	this->inputFolder = inputFolder;
	this->vstrImageFilenames = vstrImageFilenames;
	this->vTimestamps = vTimestamps;
}

void ExpertoEtiquetar::EtiquetarTesteo(){
	//Declaración de variables
	Estructura* punto;
	vector<Estructura*> allPuntos = Sistema->mpMap->GetAllEstructuras();
	Mat imgFrame;
	Mat Tcw,tf1,tf2;
	int ancho, alto, opcion;
	bool encontrado = false;
	IEstrategiaEtiquetarTesteo* estrategia = NULL;

	//Ubica las ventanas de manera que sean todas visibles
	getScreenResolution(ancho,alto);
	cv::namedWindow("Testframe 1");
	cv::moveWindow("Testframe 1",0,0);
	cv::namedWindow("Testframe 2");
	cv::moveWindow("Testframe 2",0,alto/2);

	//Elige el tipo de etiquetado a realizar
	std::vector<string> opciones{ "Automática","Manual"};
	int tipo = preguntaOpciones("Indique el tipo de etiquetado a realizar.",opciones);
	estrategia = FactoriaEstrategiasEtiquetarTesteo::Get()->CreateEstrategiaEtiquetarTesteo(tipo);

	//iteracion sobre observaciones
	vector<tuple<double, int, double, double> >::iterator obs = observaciones.begin();
	bool exito = false;
	for(vector<tuple<double, int, double, double> >::iterator fin = observaciones.end(); obs!=fin;){
		cv::namedWindow("Frame Testeo");
		cv::setMouseCallback("Frame Testeo", onMouse, (void*) &pointSelected);

		//obtener los datos de la observación para mostrar
		punto = BuscarPunto(get<1>(*obs),allPuntos);
		int cols = punto->getKf1().cols;
		int rows = punto->getKf1().rows;
		resize(punto->getKf1(),tf1,cvSize(cols*(alto/2)/rows,alto/2));
		cv::imshow("Testframe 1", tf1);
		resize(punto->getKf1(),tf2,cvSize(cols*(alto/2)/rows,alto/2));
		cv::imshow("Testframe 2", tf2);
		for(int i=0; i<vTimestamps.size(); i++){
			if(abs(get<0>(*obs)-vTimestamps[i])<0.001){//usar == con double traía errores
				imgFrame = imread(inputFolder+"/"+vstrImageFilenames[i],-1);
				encontrado=true;
				break;
			}
		}

		//Si existe la observación, se procede a etiquetar el ground truth
		if (encontrado) {
			encontrado = false;
			cv::moveWindow("Frame Testeo",ancho-imgFrame.cols,0);
			exito = estrategia->EtiquetarTesteo(obs,imgFrame);
			if(exito) obs = observaciones.erase(obs);
			else obs++; //no se eliminó ningún elemento de la lista de observaciones
		}else{//hubo un error con el timestamp del frame
			cout<<"NO SE ENCONTRÓ UN FRAME CON EL TIMESTAMP "<<get<0>(*obs)<<endl;
			cout<<"SE GUARDARÁ LA OBSERVACIÓN PARA QUE SEA EVALUADA EN OTRA OCASIÓN."<<endl;
			obs++;
		}

		//Se consulta si desea seguir etiquetando en otra ocasion
		opcion = preguntaSiNo("QUIERE SEGUIR ETIQUETANDO?", "\n\n SOLO TIENE 2 OPCIONES");
		if(opcion==2) break;
	}

	//Si no se va a seguir etiquetando, se persisten los datos necesarios para saber de donde arrancar la próxima ocasión
	cout<<"Quedan "<<observaciones.size()<<" observaciones sin etiquetar"<<endl;
	auto end = std::chrono::system_clock::now();
	std::time_t end_time = std::chrono::system_clock::to_time_t(end);
	char *endtime = std::ctime(&end_time);
	endtime[strlen(endtime) - 1] = 0;
	char fileName[100];
	sprintf(fileName, "Observacionesrestantes-%s.txt",endtime);
	ofstream f(fileName);
	obs = observaciones.begin();
	for(vector<tuple<double, int, double, double> >::iterator fin = observaciones.end(); obs!=fin; obs++){
		f << get<0>(*obs) <<"	"<< get<1>(*obs)<<"	"<< get<2>(*obs)<<"	"<< get<3>(*obs)<<"\n";
	}
	f.close();
}
