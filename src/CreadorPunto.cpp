#include <CreadorPunto3D.h>


//--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
ORB_SLAM2::KeyFrame* BuscarKF(long unsigned int kfId, vector<ORB_SLAM2::KeyFrame*> allKFs){
	vector<ORB_SLAM2::KeyFrame*>::iterator kf;
	for (kf = allKFs.begin(); kf != allKFs.end(); ++kf) {
		if ((*kf)->mnId == kfId) {
			return *kf;
		}
	}
	return static_cast<ORB_SLAM2::KeyFrame*>(NULL);
}
//---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
Estructura* BuscarPunto(int id, vector<Estructura*> puntos){
	vector<Estructura*>::iterator pun;
	for (pun = puntos.begin(); pun != puntos.end(); ++pun) {
		if ((*pun)->getId() == id) {
			return *pun;
		}
	}
	return static_cast<Estructura*>(NULL);
}

//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
void drawPoint( cv::Mat image, const cv::Point2d punto, cv::Mat& outImage,const cv::Scalar& color, int radius){

	cv::Point pt1;
	if (image.channels() < 3) {
		cv::cvtColor(image, image, cv::COLOR_GRAY2BGR);
	}
	circle(image, punto, radius, color, 2, 8, 0);

	outImage = image.clone();

}


//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
double distanciaPuntoPunto(cv::Mat punto1, cv::Mat punto2){
	return sqrt( pow(punto1.at<float>(0)-punto2.at<float>(0),2) +
				 pow(punto1.at<float>(1)-punto2.at<float>(1),2) +
				 pow(punto1.at<float>(2)-punto2.at<float>(2),2));
}
//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
cv::Vec3d encontrarPuntoDistancia(cv::Vec3d director, cv::Vec3d punto, double distancia){
	double lambda = distancia/norm(director);
	//obtiene el punto mediante la ecuacion de la recta
	cv::Vec3d salida = punto + lambda*director;
	return salida;
}


