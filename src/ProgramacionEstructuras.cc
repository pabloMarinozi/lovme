/*
 * ProgramacionEstructuras.cc
 *
 *  Created on: 24 feb. 2019
 *      Author: pablo
 */

#include "ProgramacionEstructuras.h"
#include "CreadorPunto3D.h"
#include "InterfazUsuario.h"
#include "Math.h"


bool clickPrimer = true;
bool clickPause = false;
ORB_SLAM2::KeyFrame* kF1;
ORB_SLAM2::KeyFrame* kF2;
cv::Point p1,p2;

cv::Point2d ReproyectarPunto(cv::Mat R, cv::Mat t, cv::Mat K, cv::Mat p3d){
	float fx = K.at<float>(0,0);
	float fy = K.at<float>(1,1);
	float cx = K.at<float>(0,2);
	float cy = K.at<float>(1,2);

	float z = R.row(2).dot(p3d) + t.at<float>(2);
	float invz1 = 1.0 / z;
	float x = R.row(0).dot(p3d) + t.at<float>(0);
	float y = R.row(1).dot(p3d) + t.at<float>(1);
	float u = fx * x * invz1 + cx;
	float v = fy * y * invz1 + cy;

	return cv::Point2d(u,v);
}

//////////////////////////////////////////Implementación de la clase Estructura///////////////////////////////////////////////////////////
cv::Point2d Estructura::reproyectarPunto(ORB_SLAM2::MapPoint* hp, ORB_SLAM2::KeyFrame* kf){
	cv::Mat p3d = hp->GetWorldPos().t();
	cv::Mat R = kf->GetRotation();
	cv::Mat t = kf->GetTranslation();
	cv::Mat K = kf->mK;
	return ReproyectarPunto(R,t,K,p3d);
}

cv::Point2d Estructura::reproyectarPunto(ORB_SLAM2::MapPoint* hp, ORB_SLAM2::Frame f){
	cv::Mat p3d = hp->GetWorldPos().t();
	cv::Mat R = f.getTcw().rowRange(0,3).colRange(0,3).clone();
	cv::Mat t = f.getTcw().rowRange(0,3).col(3).clone();
	cv::Mat K = f.mK;
	return  ReproyectarPunto(R,t,K,p3d);
}

cv::Point2d Estructura::reproyectarPunto(ORB_SLAM2::MapPoint* hp, cv::Mat Tcw, cv::Mat K){
	cv::Mat p3d = hp->GetWorldPos().t();
	cv::Mat R = Tcw.rowRange(0,3).colRange(0,3).clone();
	cv::Mat t = Tcw.rowRange(0,3).col(3).clone();
	return  ReproyectarPunto(R,t,K,p3d);
}

vector<Point2d> Estructura::Reproyectar(cv::Mat Tcw, cv::Mat K){
	vector<Point2d> v;
	for(int i = 0; i<puntos.size();i++){
		v.push_back(reproyectarPunto(puntos[i], Tcw, K));
	}
	return v;
}

vector<Point2d> Estructura::Reproyectar(ORB_SLAM2::KeyFrame* kf){
	vector<Point2d> v;
	for(int i = 0; i<puntos.size();i++){
		v.push_back(reproyectarPunto(puntos[i], kf));
	}
	return v;
}

vector<Point2d> Estructura::Reproyectar(ORB_SLAM2::Frame f){
	vector<Point2d> v;
	for(int i = 0; i<puntos.size();i++){
		v.push_back(reproyectarPunto(puntos[i], f));
	}
	return v;
}

ORB_SLAM2::MapPoint* Estructura::TriangularHotpoint(pair<long unsigned int, cv::Point> kp1,
		pair<long unsigned int, cv::Point> kp2, vector<ORB_SLAM2::KeyFrame*> allKFs, ORB_SLAM2::Map* mapa) {

	try {
		//Recupera ambos keyframes
		ORB_SLAM2::KeyFrame* pKF1 = kF1 = BuscarKF(kp1.first,allKFs);
		ORB_SLAM2::KeyFrame* pKF2 = kF2 = BuscarKF(kp2.first,allKFs);
		p1 = kp1.second;
		p2 = kp2.second;

		//Recupera los datos de los keyframes que necesita para triangular
		cv::Mat Rcw1 = pKF1->GetRotation();
		cv::Mat Rwc1 = Rcw1.t();
		cv::Mat tcw1 = pKF1->GetTranslation();
		cv::Mat Tcw1(3,4,CV_32F);
		Rcw1.copyTo(Tcw1.colRange(0,3));
		tcw1.copyTo(Tcw1.col(3));
		cv::Mat Ow1 = pKF1->GetCameraCenter();
		cv::Mat K1 = pKF1->mK;

		const float &fx1 = K1.at<float>(0,0);
		const float &fy1 = K1.at<float>(1,1);
		const float &cx1 = K1.at<float>(0,2);
		const float &cy1 = K1.at<float>(1,2);
		const float &invfx1 = 1/fx1;
		const float &invfy1 = 1/fy1;

		const float ratioFactor = 1.5f*pKF1->mfScaleFactor;

		cv::Mat Rcw2 = pKF2->GetRotation();
		cv::Mat Rwc2 = Rcw2.t();
		cv::Mat tcw2 = pKF2->GetTranslation();
		cv::Mat Tcw2(3, 4, CV_32F);
		Rcw2.copyTo(Tcw2.colRange(0, 3));
		tcw2.copyTo(Tcw2.col(3));

		cv::Mat K2 = pKF2->mK;

		const float &fx2 = K2.at<float>(0,0);
		const float &fy2 = K2.at<float>(1,1);
		const float &cx2 = K2.at<float>(0,2);
		const float &cy2 = K2.at<float>(1,2);
		const float &invfx2 = 1/fx2;
		const float &invfy2 = 1/fy2;

		// Check first that baseline is not too short
		cv::Mat Ow2 = pKF2->GetCameraCenter();
		cv::Mat vBaseline = Ow2 - Ow1;
		const float baseline = cv::norm(vBaseline);

		/*if(!mbMonocular)
		 {
		 if(baseline<pKF2->mb)
		 continue;
		 }
		 else*/
		{
			const float medianDepthKF2 = pKF2->ComputeSceneMedianDepth(2);
			const float ratioBaselineDepth = baseline / medianDepthKF2;

			if (ratioBaselineDepth < 0.01){
				cout<<"Falló la triangulación porque el ratioBaselineDepth < 0.01"<<endl;
				return static_cast<ORB_SLAM2::MapPoint*>(NULL);
			}
		}

		// Compute Fundamental Matrix
		cv::Mat F12 = ComputeF12(pKF1, pKF2);

		/*const cv::KeyPoint &kp1 = mpCurrentKeyFrame->mvKeysUn[idx1];
			const float kp1_ur = mpCurrentKeyFrame->mvuRight[idx1];


			const cv::KeyPoint &kp2 = pKF2->mvKeysUn[idx2];
			const float kp2_ur = pKF2->mvuRight[idx2];
		 */

		//seteo los booleanos en false porque son frames monoculares
		bool bStereo1 = false;
		bool bStereo2 = false;

		// Check parallax between rays
		cv::Mat xn1 =
				(cv::Mat_<float>(3, 1) << (kp1.second.x - cx1) * invfx1, (kp1.second.y
						- cy1) * invfy1, 1.0);
		cv::Mat xn2 =
				(cv::Mat_<float>(3, 1) << (kp2.second.x - cx2) * invfx2, (kp2.second.y
						- cy2) * invfy2, 1.0);

		cv::Mat ray1 = Rwc1 * xn1;
		cv::Mat ray2 = Rwc2 * xn2;
		const float cosParallaxRays = ray1.dot(ray2)
									/ (cv::norm(ray1) * cv::norm(ray2));

		float cosParallaxStereo = cosParallaxRays + 1;
		float cosParallaxStereo1 = cosParallaxStereo;
		float cosParallaxStereo2 = cosParallaxStereo;

		cosParallaxStereo = min(cosParallaxStereo1, cosParallaxStereo2);

		cv::Mat x3D;
		if (cosParallaxRays < cosParallaxStereo && cosParallaxRays > 0
				&& (bStereo1 || bStereo2 || cosParallaxRays < 0.9998)) {
			// Linear Triangulation Method
			cv::Mat A(4, 4, CV_32F);
			A.row(0) = xn1.at<float>(0) * Tcw1.row(2) - Tcw1.row(0);
			A.row(1) = xn1.at<float>(1) * Tcw1.row(2) - Tcw1.row(1);
			A.row(2) = xn2.at<float>(0) * Tcw2.row(2) - Tcw2.row(0);
			A.row(3) = xn2.at<float>(1) * Tcw2.row(2) - Tcw2.row(1);

			cv::Mat w, u, vt;
			cv::SVD::compute(A, w, u, vt, cv::SVD::MODIFY_A | cv::SVD::FULL_UV);

			x3D = vt.row(3).t();

			if (x3D.at<float>(3) == 0)
				cout<<"la ultima componente es 0"<<endl;
			// Euclidean coordinates
			x3D = x3D.rowRange(0, 3) / x3D.at<float>(3);

		}

		cv::Mat x3Dt = x3D.t();

		//Check triangulation in front of cameras
		float z1 = Rcw1.row(2).dot(x3Dt) + tcw1.at<float>(2);
		if (z1 <= 0){
			cout<<"\n\n\n LA TRIANGULACIÓN FALLÓ"<<endl;
			cout<<"El MapPoint quedó ubicado detrás del primer keyframe\n"<<endl;
			cout<<"Se recomienda intentar con otros keyframes"<<endl;
			return static_cast<ORB_SLAM2::MapPoint*>(NULL);
		}

		float z2 = Rcw2.row(2).dot(x3Dt) + tcw2.at<float>(2);
		if (z2 <= 0){
			cout<<"\n\n\n LA TRIANGULACIÓN FALLÓ"<<endl;
			cout<<"El MapPoint quedó ubicado detrás del segundo keyframe\n"<<endl;
			cout<<"Se recomienda intentar con otros keyframes"<<endl;
			return static_cast<ORB_SLAM2::MapPoint*>(NULL);
		}

		//Check reprojection error in first keyframe
		//const float &sigmaSquare1 = pKF1->mvLevelSigma2[kp1.octave];
		const float x1 = Rcw1.row(0).dot(x3Dt) + tcw1.at<float>(0);
		const float y1 = Rcw1.row(1).dot(x3Dt) + tcw1.at<float>(1);
		const float invz1 = 1.0 / z1;

		if (!bStereo1) {
			float u1 = fx1 * x1 * invz1 + cx1;
			float v1 = fy1 * y1 * invz1 + cy1;
			float errX1 = u1 - kp1.second.x;
			float errY1 = v1 - kp1.second.y;
			cout<<"error1: "<<errX1<<","<<errY1<<endl;
			//if ((errX1 * errX1 + errY1 * errY1) > 5.991 * sigmaSquare1)
			//continue;
		} else {
			float u1 = fx1 * x1 * invz1 + cx1;
			//float u1_r = u1 - mpCurrentKeyFrame->mbf*invz1;
			float v1 = fy1 * y1 * invz1 + cy1;
			float errX1 = u1 - kp1.second.x;
			float errY1 = v1 - kp1.second.y;
			cout<<"error2: "<<errX1<<","<<errY1<<endl;
			//float errX1_r = u1_r - kp1_ur;
			//if((errX1*errX1+errY1*errY1+errX1_r*errX1_r)>7.8*sigmaSquare1)
			//continue;
		}

		//Check reprojection error in second keyframe
		//const float sigmaSquare2 = pKF2->mvLevelSigma2[kp2.octave];
		const float x2 = Rcw2.row(0).dot(x3Dt) + tcw2.at<float>(0);
		const float y2 = Rcw2.row(1).dot(x3Dt) + tcw2.at<float>(1);
		const float invz2 = 1.0 / z2;
		if (!bStereo2) {
			float u2 = fx2 * x2 * invz2 + cx2;
			float v2 = fy2 * y2 * invz2 + cy2;
			float errX2 = u2 - kp2.second.x;
			float errY2 = v2 - kp2.second.y;
			cout<<"error: "<<errX2<<","<<errY2<<endl;
			cout<<"punto: "<<u2<<", "<<v2<<endl;
			//if ((errX2 * errX2 + errY2 * errY2) > 5.991 * sigmaSquare2)
			//continue;
		} else {
			float u2 = fx2 * x2 * invz2 + cx2;
			//float u2_r = u2 - mpCurrentKeyFrame->mbf*invz2;
			float v2 = fy2 * y2 * invz2 + cy2;
			float errX2 = u2 - kp2.second.x;
			float errY2 = v2 - kp2.second.y;
			cout<<"error: "<<errX2<<", "<<errY2<<endl;
			cout<<"punto: "<<u2<<", "<<v2<<endl;
			//float errX2_r = u2_r - kp2_ur;
			//if((errX2*errX2+errY2*errY2+errX2_r*errX2_r)>7.8*sigmaSquare2)
			//continue;
		}

		//Check scale consistency
		cv::Mat normal1 = x3D - Ow1;
		float dist1 = cv::norm(normal1);

		cv::Mat normal2 = x3D - Ow2;
		float dist2 = cv::norm(normal2);

		/*if (dist1 == 0 || dist2 == 0)
				continue;

			const float ratioDist = dist2 / dist1;
			//const float ratioOctave = pKF1->mvScaleFactors[kp1.octave]
					/ pKF2->mvScaleFactors[kp2.octave];

			if(fabs(ratioDist-ratioOctave)>ratioFactor)
			 continue;
			if (ratioDist * ratioFactor < ratioOctave
					|| ratioDist > ratioOctave * ratioFactor)
				continue;*/
		cout<<"TRIANGULACION EXITOSA!"<<endl;
		// Triangulation is succesfull
		return new ORB_SLAM2::MapPoint(x3D, pKF1,mapa);
	} catch (cv::Exception e) {
		cout << e.what() << endl;
		return static_cast<ORB_SLAM2::MapPoint*>(NULL);
	}
}

cv::Mat Estructura::ComputeF12(ORB_SLAM2::KeyFrame *&pKF1, ORB_SLAM2::KeyFrame *&pKF2)
{
    cv::Mat R1w = pKF1->GetRotation();
    cv::Mat t1w = pKF1->GetTranslation();
    cv::Mat R2w = pKF2->GetRotation();
    cv::Mat t2w = pKF2->GetTranslation();

    cv::Mat R12 = R1w*R2w.t();
    cv::Mat t12 = -R1w*R2w.t()*t2w+t1w;

    cv::Mat t12x = SkewSymmetricMatrix(t12);

    const cv::Mat &K1 = pKF1->mK;
    const cv::Mat &K2 = pKF2->mK;


    return K1.t().inv()*t12x*R12*K2.inv();
}
cv::Mat Estructura::SkewSymmetricMatrix(const cv::Mat &v)
{
    return (cv::Mat_<float>(3,3) <<             0, -v.at<float>(2), v.at<float>(1),
            v.at<float>(2),               0,-v.at<float>(0),
            -v.at<float>(1),  v.at<float>(0),              0);
}

//////////////////////////////////////////Implementación de la clase Punto///////////////////////////////////////////////////////////
Punto::Punto(int n){
	vector<ORB_SLAM2::KeyFrame*> allKFs = Sistema->mpMap->GetAllKeyFrames();
	cv::Mat im1,im2;
	vector<cv::Point> puntos;
	long unsigned int kfId;
	vector<ORB_SLAM2::KeyFrame*> excluidos;
	pair<long unsigned int, cv::Point> par1, par2;
	vector<ORB_SLAM2::MapPoint*> extremos;

	//Obtengo un punto del primer keyframe
	do {
		kfId = ElegirKeyFrame(excluidos,Sistema);
		if(kfIds.size()<2){
			kfIds.push_back(kfId);
		}else{
			kfIds.clear();
			kfIds.push_back(kfId);
		}

		puntos = ElegirPuntos(1,kfId,Sistema,im1);
	} while (puntos.size()==0);
	par1 = make_pair(kfId,puntos[0]);

	//Obtengo un punto del segundo keyframe
	puntos.clear();
	excluidos.push_back(BuscarKF(kfId, allKFs));
	kfId = ElegirKeyFrame(excluidos,Sistema);
	if(kfIds.size()<2){
		kfIds.push_back(kfId);
	}else{
		kfIds.clear();
		kfIds.push_back(kfId);
	}
	do {
		puntos = ElegirPuntos(1,kfId,Sistema, im2);
	} while (puntos.size()==0);
	par2 = make_pair(kfId,puntos[0]);

	//Calculo el punto 3d
	ORB_SLAM2::MapPoint* pMP = TriangularHotpoint(par1,par2,allKFs, Sistema->mpMap);
	if(pMP){
		pMP->isHotpoint = true;
		this->puntos.push_back(pMP);
	}else
		this->puntos.push_back(static_cast<ORB_SLAM2::MapPoint*>(NULL));

	Sistema->mpMap->MaxEstructuraId++;
	id = Sistema->mpMap->MaxEstructuraId;
	kf1 = im1;
	kf2 = im2;
}

bool Punto::enPantalla(ORB_SLAM2::Frame f){
	cv::Mat Tcw = f.getTcw();
	cv::Mat Rcw1 = Tcw.rowRange(0,3).colRange(0,3);
	cv::Mat tcw1 = Tcw.rowRange(0,3).col(3);
	cv::Mat x3D1 = puntos[0]->GetWorldPos();
	cv::Mat x3Dt1 = x3D1.t();
	float z1 = Rcw1.row(2).dot(x3Dt1) + tcw1.at<float>(2);
	return z1>0;
}

bool Punto::enPantalla(ORB_SLAM2::KeyFrame* kf){
	cv::Mat Rcw1 = kf->GetRotation();
	cv::Mat tcw1 = kf->GetTranslation();
	cv::Mat x3D1 = puntos[0]->GetWorldPos();
	cv::Mat x3Dt1 = x3D1.t();
	float z1 = Rcw1.row(2).dot(x3Dt1) + tcw1.at<float>(2);
	return z1>0;
}

cv::Mat Punto::reproyectar(ORB_SLAM2::Frame f, cv::Mat img){
	cv::Point2d punto = reproyectarPunto(this->puntos[0],f);
	cv::Scalar color(0,255,255);
	circle(img, punto, 5, color, -1);
	return img;
}

cv::Mat Punto::reproyectar(ORB_SLAM2::KeyFrame* kf, cv::Mat img){
	cv::Point2d punto = reproyectarPunto(this->puntos[0],kf);
	cv::Scalar color(0,255,255);
	circle(img, punto, 5, color, -1);
	return img;
}

//////////////////////////////////////////Implementación de la clase Linea///////////////////////////////////////////////////////////
Linea::Linea(int n){
	vector<ORB_SLAM2::KeyFrame*> allKFs = Sistema->mpMap->GetAllKeyFrames();
	cv::Mat im1,im2;
	long unsigned int kfId;
	vector<cv::Point> puntos;
	vector<ORB_SLAM2::KeyFrame*> excluidos;
	vector<pair<long unsigned int, cv::Point> > pares1, pares2;
	vector<ORB_SLAM2::MapPoint*> extremos;

	//Obtengo dos puntos del primer keyframe
	kfId = ElegirKeyFrame(excluidos,Sistema);
	if(kfIds.size()<2){
		kfIds.push_back(kfId);
	}else{
		kfIds.clear();
		kfIds.push_back(kfId);
	}
	do {
		puntos = ElegirPuntos(2,kfId,Sistema,im1);
	} while (puntos.size()==0);
	pares1.push_back(make_pair(kfId,puntos[0]));
	pares1.push_back(make_pair(kfId,puntos[1]));

	//Obtengo dos puntos del segundo keyframe
	puntos.clear();
	excluidos.push_back(BuscarKF(kfId, allKFs));
	kfId = ElegirKeyFrame(excluidos,Sistema);
	if(kfIds.size()<2){
		kfIds.push_back(kfId);
	}else{
		kfIds.clear();
		kfIds.push_back(kfId);
	}
	do {
		puntos = ElegirPuntos(2,kfId,Sistema,im2);
	} while (puntos.size()==0);
	pares2.push_back(make_pair(kfId,puntos[0]));
	pares2.push_back(make_pair(kfId,puntos[1]));

	//Calculo ambos puntos 3d
	for (int var = 0; var < 2; ++var) {
		ORB_SLAM2::MapPoint* pMP = TriangularHotpoint(pares1[var],pares2[var],allKFs, Sistema->mpMap);
		if(pMP){
			pMP->isHotpoint = true;
			extremos.push_back(pMP);
		}
	}

	if (extremos.size() == 2){
		this->puntos.push_back(extremos[0]);
		this->puntos.push_back(extremos[1]);
	}else
		this->puntos.push_back(static_cast<ORB_SLAM2::MapPoint*>(NULL));

	Sistema->mpMap->MaxEstructuraId++;
	id = Sistema->mpMap->MaxEstructuraId;
	kf1 = im1;
	kf2 = im2;
}

bool Linea::enPantalla(ORB_SLAM2::Frame f){
	cv::Mat Tcw = f.getTcw();
	cv::Mat Rcw1 = Tcw.rowRange(0,3).colRange(0,3);
	cv::Mat tcw1 = Tcw.rowRange(0,3).col(3);
	cv::Mat x3D1 = puntos[0]->GetWorldPos();
	cv::Mat x3D2 = puntos[1]->GetWorldPos();
	cv::Mat x3Dt1 = x3D1.t();
	cv::Mat x3Dt2 = x3D2.t();
	float z1 = Rcw1.row(2).dot(x3Dt1) + tcw1.at<float>(2);
	float z2 = Rcw1.row(2).dot(x3Dt2) + tcw1.at<float>(2);
	return z1>0 && z2>0;
}

bool Linea::enPantalla(ORB_SLAM2::KeyFrame* kf){
	cv::Mat Rcw1 = kf->GetRotation();
	cv::Mat tcw1 = kf->GetTranslation();
	cv::Mat x3D1 = puntos[0]->GetWorldPos();
	cv::Mat x3D2 = puntos[1]->GetWorldPos();
	cv::Mat x3Dt1 = x3D1.t();
	cv::Mat x3Dt2 = x3D2.t();
	float z1 = Rcw1.row(2).dot(x3Dt1) + tcw1.at<float>(2);
	float z2 = Rcw1.row(2).dot(x3Dt2) + tcw1.at<float>(2);
	return z1>0 && z2>0;
}

cv::Mat Linea::reproyectar(ORB_SLAM2::KeyFrame* kf, cv::Mat img){
	cv::Point2d punto1 = reproyectarPunto(this->puntos[0],kf);
	cv::Point2d punto2 = reproyectarPunto(this->puntos[1],kf);
	cv::Scalar color(255,255,0);
	/*cv::Vec2d director = directorRecta2D(punto1, punto2);

	for(int i=0; i<9; i+=2){
		cv::Point2d puntoA,puntoB;
		puntoA.x = punto1.x + director[0]*i/10;
		puntoA.y = punto1.y + director[1]*i/10;
		puntoB.x = punto1.x + director[0]*(i+1)/10;
		puntoB.y = punto1.y + director[1]*(i+1)/10;
		line(img, puntoA, puntoB, color, 5);
	}*/
	line(img, punto1, punto2, color, 5);
	return img;
}

cv::Vec2d directorRecta2D(cv::Point2d punto1, cv::Point2d punto2){
	cv::Vec2d director;
	director[0] = punto2.x-punto1.x;
	director[1] = punto2.y-punto1.y;
	return director;
}

cv::Mat Linea::reproyectar(ORB_SLAM2::Frame f, cv::Mat img){
	cv::Point2d punto1 = reproyectarPunto(this->puntos[0],f);
	cv::Point2d punto2 = reproyectarPunto(this->puntos[1],f);
	cv::Vec2d director = directorRecta2D(punto1, punto2);
	cv::Scalar color(0,255,255);
	int partes = 6;
	for(int i=0; i<partes-1; i+=2){
		cv::Point2d puntoA,puntoB;
		puntoA.x = punto1.x + director[0]*i/partes;
		puntoA.y = punto1.y + director[1]*i/partes;
		puntoB.x = punto1.x + director[0]*(i+1)/partes;
		puntoB.y = punto1.y + director[1]*(i+1)/partes;
		arrowedLine(img, puntoB, puntoA, color, 3,7,0,0.4);
	}
	return img;
}

//////////////////////////////////////////Implementación de la clase FactoriaEstructura///////////////////////////////////////////////////////////
void FactoriaEstructuras::Register(const int tipoMetodo, CreateEstructuraFn pfnCreate)
{
    m_FactoryMap[tipoMetodo] = pfnCreate;
}

FactoriaEstructuras::FactoriaEstructuras(){
	Register(1, &Punto::Create);
	Register(2, &Linea::Create);
}

Estructura *FactoriaEstructuras::CreateEstructura(const int tipoMetodo)
{
	FactoryMap::iterator it = m_FactoryMap.find(tipoMetodo);
	if( it != m_FactoryMap.end() )
		return it->second();
	return NULL;
}

//////////////////////////////////////////Implementación de la clase ExpertoEstructuras///////////////////////////////////////////////////////////
bool ExpertoEstructuras::ProgramarEstructura(){
	//declaracion de variables
	bool cancelado = false;
	bool aceptado = false;
	vector<cv::Mat> reproyecciones;
	vector<ORB_SLAM2::KeyFrame*> allKFs = Sistema->mpMap->GetAllKeyFrames();
	vector<ORB_SLAM2::KeyFrame*>::iterator kf;

	//Ingresar puntos
	std::vector<string> opciones = {"Punto","Linea"};
	int respuesta = preguntaOpciones("¿Qué tipo de estructura desea programar?",opciones);
	Estructura * estructura = FactoriaEstructuras::Get()->CreateEstructura(respuesta);


	//Calcular keyframes para reproyectar
	if(estructura->getPuntos()[0]){
		//CALCULOS PARA SELECCIONAR EL GRUPO DE KEYFRAMES SOBRE LOS QUE SE REPROYECTARÁ LA ESTRUCTURA
		int j=0;int i1=0;int i2=0;int iMenor,iMayor;
		for (kf = allKFs.begin(); kf != allKFs.end(); ++kf) {
			if ((*kf)->mnId == kfIds[0]) i1=j;
			if ((*kf)->mnId == kfIds[1]) i2=j;
			j++;
		}
		if(i2-i1<0){iMayor=i1;iMenor=i2;}
		else{iMayor=i2;iMenor=i1;}
		j=0;

		//REPROYECCIÓN DE LA ESTRUCTURA SOBRE EL GRUPO DE KEYFRAMES CALCULADO
		for (kf = allKFs.begin(); kf != allKFs.end(); ++kf) {
			if ((*kf)->mnId != 0 && j>iMenor-5 && j<iMayor+5) {
				reproyecciones.push_back( estructura->reproyectar(*kf, (*kf)->getImGray()));
			}
			j++;
		}
	}else return false;

	if (reproyecciones.size()>0) {
		//SE MUESTRAN POR PANTALLA LAS REPROYECCIONES
		cv::namedWindow("Reproyecciones");
		cv::setMouseCallback("Reproyecciones", onMouse2, (void*) &pointSelected);
		int i = 0;
		cout<<"\nSe muestran por pantalla las reproyecciones del punto creado.\n"
				"Presione click izquierdo sobre la ventana si está satisfecho con las reproyecciones.\n"
				"Presione click derecho sobre la ventana si no está satisfecho con las reproyecciones y desea programa la linea nuevamente"<<endl;
		while(!aceptado && !cancelado){
			cv::Mat img = reproyecciones[i];
			cv::imshow("Reproyecciones",img);
			if(eventLeftClick) {
				aceptado = true;
				eventLeftClick = false;
			}
			if(eventRightClick){
				cancelado = true;
				eventRightClick = false;
			}
			if(i<reproyecciones.size()-1) i++;
			else i=0;
			sleep(1);
		}

		cv::destroyWindow("Reproyecciones");

		//CONFIRMACIÓN DE LA ESTRUCTURA
		if (aceptado) {
			string descripcion,descripcion2;
			cout << "Ingrese la descripcion del punto que acaba de confirmar."<<endl;
			getline(cin,descripcion);
			while(descripcion.length() == 0){
				getline(cin,descripcion);
			}
			estructura->setDescripcion(descripcion);
			estructura->setVideo(videoId);
			if(estructura->getPuntos().size()==1)
				Sistema->mpMap->AddEstructura(estructura);
			cout<<"Se ha aceptado el punto. "<<endl;
			vector<Estructura*> estructuras = Sistema->mpMap->GetAllEstructuras();
			cout<<"Hasta ahora el mapa tiene "<<estructuras.size()<<" estructuras."<<endl;
			for(int i=0; i<estructuras.size(); i++){
				cout<<estructuras[i]->getId()<<": "<<estructuras[i]->getDescripcion()<<endl;
			}
		}


		if(cancelado){
			cout<<"Se ha cancelado la creación del punto."<<endl;
			return false;
		}
		return true;
	}else{
		cout<<"\n LAS REPROYECCIONES FALLARON. DEBERÁ INGRESAR LOS PUNTOS NUEVAMENTE \n";
		return false;
	}
}

void ExpertoEstructuras::EliminarEstructura(string mapFile,ORB_SLAM2::System* Sistema){
	vector<Estructura*> puntos = Sistema->mpMap->GetAllEstructuras();
	int idPunto;
	if(puntos.size()==0){
		cout<<"\n NO HAY PUNTOS EN ESTE MAPA!!"<<endl;
		exit(0);
	}
	cout<< "\n Ingrese el id que desea eliminar"<<endl;
	int seguro;
	for(int i = 0; i<puntos.size();i++){
		cout<<puntos[i]->getId()<<": "<<puntos[i]->getDescripcion()<<endl;
	}
	cin >> idPunto;
	while (cin.fail() ||!BuscarPunto(idPunto,puntos)) {
		cin.clear(); // clear input buffer to restore cin to a usable state
		cin.ignore(INT_MAX, '\n'); // ignore last input
		cout << "NO EXISTE ESE PUNTO. Intente de nuevo\n";
		for(int i = 0; i<puntos.size();i++){
			cout<<puntos[i]->getId()<<": "<<puntos[i]->getDescripcion()<<endl;
		}
		cin >> idPunto;
	}
	Estructura* punto_borrado = BuscarPunto(idPunto,puntos);
	preguntaSiNo("¿Está seguro que desea eliminar el ṕunto "+to_string(punto_borrado->getId())+"?","\n\nSOLO PUEDES INGRESAR NUMEROS QUE CORRESPONDAN CON ALGUNA OPCION.\n");
	switch(seguro){
	case 1:
		Sistema->mpMap->EraseEstructura(punto_borrado);
		Sistema->serializer->mapSave(mapFile);
		cout<<"Punto borrado exitosamente."<<endl;
		break;
	case 2:
		cout<<"Eliminación cancelada."<<endl;
		break;
	}
}

void ExpertoEstructuras::EditarEstructura(string mapFile,ORB_SLAM2::System* Sistema){
	vector<Estructura*> puntos = Sistema->mpMap->GetAllEstructuras();
	if(puntos.size()==0){
		cout<<"\n NO HAY PUNTOS EN ESTE MAPA!!"<<endl;
		exit(0);
	}
	cout<<"Elija el punto que quiere editar:"<<endl;
	int idPunto;
	for(int i = 0; i<puntos.size();i++){
		cout<<puntos[i]->getId()<<": "<<puntos[i]->getDescripcion()<<endl;
	}
	Estructura* punto_editado;
	string descripcion;
	cin>>idPunto;
	while (cin.fail() ||!BuscarPunto(idPunto,puntos)) {
		cin.clear(); // clear input buffer to restore cin to a usable state
		cin.ignore(INT_MAX, '\n'); // ignore last input
		cout << "\n\nSOLO PUEDES INGRESAR NUMEROS QUE CORRESPONDAN CON ALGUNA OPCION.\n";
		cout<<"Elija el punto que quiere editar:"<<endl;
		for(int i = 0; i<puntos.size();i++){
			cout<<puntos[i]->getId()<<": "<<puntos[i]->getDescripcion()<<endl;
		}
		cin>>idPunto;
	}
	punto_editado = BuscarPunto(idPunto,puntos);
	cout << "Ingrese la nueva descripcion del punto."<<endl;
	getline(cin,descripcion);
	while(descripcion.length() == 0){
		getline(cin,descripcion);
	}
	punto_editado->setDescripcion(descripcion);
	cout << "Ingrese el id del video del que se obtuvo el punto. Hasta ahora su valor era "<<punto_editado->getVideo()<<endl;
	int videoNuevo;
	cin>>videoNuevo;
	while (cin.fail() || videoNuevo<=0) {
		cin.clear(); // clear input buffer to restore cin to a usable state
		cin.ignore(INT_MAX, '\n'); // ignore last input
		cout << "SOLO PUEDE INGRESAR VALORES POSITIVOS"<<endl;
		cout<<"Ingrese el id del video del que se obtuvo el punto. Hasta ahora su valor era "<<punto_editado->getVideo()<<endl;
		cin>>videoNuevo;
	}
	punto_editado->setVideo(videoNuevo);
	Sistema->serializer->mapSave(mapFile);
}

double ExpertoEstructuras::CalcularEscala(){
	bool cancelado = false;
	bool aceptado = false;
	vector<Estructura*> puntos;
	vector<cv::Mat> reproyecciones;
	string str;
	cv::Mat im1, im2;
	int iMenor,iMayor;
	vector<ORB_SLAM2::KeyFrame*> allKFs = Sistema->mpMap->GetAllKeyFrames();
	vector<ORB_SLAM2::KeyFrame*>::iterator kf;

	///////////////////////////////////////////////USUARIO INGRESA PUNTOS///////////////////////////////////////////////////
	while (puntos.size() != 2) {
		cout<<"\n\nSe solicitaran los datos para un nuevo punto\n";
		sleep(1);
		Estructura* punto = FactoriaEstructuras::Get()->CreateEstructura(1);;

		/////////////////////////////////////////////REPROYECCIONES//////////////////////////////////////////////////////////////////
		if(punto){
			//CALCULOS PARA SELECCIONAR EL GRUPO DE KEYFRAMES SOBRE LOS QUE SE REPROYECTARÁ LA ESTRUCTURA
			int j=0;int i1=0;int i2=0;
			for (kf = allKFs.begin(); kf != allKFs.end(); ++kf) {
				if ((*kf)->mnId == kfIds[0]) i1=j;
				if ((*kf)->mnId == kfIds[1]) i2=j;
				j++;
			}
			if(i2-i1<0){iMayor=i1;iMenor=i2;}
			else{iMayor=i2;iMenor=i1;}
			j=0;

			//REPROYECCIÓN DE LA ESTRUCTURA SOBRE EL GRUPO DE KEYFRAMES CALCULADO
			for (kf = allKFs.begin(); kf != allKFs.end(); ++kf) {
				if ((*kf)->mnId != 0 && j>iMenor-5 && j<iMayor+5) {
					reproyecciones.push_back( punto->reproyectar(*kf, (*kf)->getImGray()));
				}
				j++;
			}

			if (reproyecciones.size()>0) {
				//SE MUESTRAN POR PANTALLA LAS REPROYECCIONES
				cv::namedWindow("Reproyecciones");
				cv::setMouseCallback("Reproyecciones", onMouse2, (void*) &pointSelected);
				int i = 0;
				cout<<"\nSe muestran por pantalla las reproyecciones del punto creado.\n"
						"Presione click izquierdo sobre la ventana si está satisfecho con las reproyecciones.\n"
						"Presione click derecho sobre la ventana si no está satisfecho con las reproyecciones y desea programa la linea nuevamente"<<endl;
				while(!aceptado && !cancelado){
					cv::Mat img = reproyecciones[i];
					cv::imshow("Reproyecciones",img);
					if(eventLeftClick) {
						aceptado = true;
						im1 = img;
						eventLeftClick = false;
					}
					if(eventRightClick){
						cancelado = true;
						eventRightClick = false;
					}
					if(i<reproyecciones.size()-1) i++;
					else i=0;
					sleep(1);
				}

				//CONFIRMACIÓN DE LA ESTRUCTURA
				if (aceptado) {
					puntos.push_back(punto);
					aceptado = false;
				}
				cv::destroyWindow("Reproyecciones");
				reproyecciones.clear();
				if(cancelado){
					cout<<"SE HA CANCELADO EL PUNTO SELECCIONADO, INTENTE DE NUEVO"<<endl;
					cancelado = false;
				}
			}else{
				cout<<"\n LAS REPROYECCIONES FALLARON. DEBERÁ INGRESAR LOS PUNTOS NUEVAMENTE \n";
			}
		}else{
			cout<<"ALGO FALLÓ EN LA CREACIÓN DEL PUNTO, INTENTE DE NUEVO"<<endl;
		}
	}

	///////////////////////////////////////////////SE CALCULA DISTANCIA ENTRE AMBOS PUNTOS///////////////////////////////////////////////////
	Mat p1 = puntos[0]->getPuntos()[0]->GetWorldPos();
	Mat p2 = puntos[1]->getPuntos()[0]->GetWorldPos();

	double distancia = sqrt( pow(p1.at<float>(0)-p2.at<float>(0),2) +
			 pow(p1.at<float>(1)-p2.at<float>(1),2)+ pow(p1.at<float>(2)-p2.at<float>(2),2));

	ORB_SLAM2::KeyFrame* keyf = BuscarKF(kfIds[0],allKFs);
	Mat imgEscala;
	if (keyf) {
		imgEscala =  keyf->getImGray();
		line(imgEscala, puntos[0]->Reproyectar(keyf)[0],
				puntos[1]->Reproyectar(keyf)[0],
				cv::Scalar(255, 255, 255),5,cv::LINE_4);
	}else cout<<"HUBO UN PROBLEMA AL MOSTRAR LA DISTANCIA A MEDIR"<<endl;
	cv::namedWindow("Escala");
	imshow("Escala", imgEscala);
	double escala = solicitaRealDoblePrecision("Ingrese la distancia real en metros entre los dos puntos que se muestran en la ventana 'Escala'",0);
	cout<< "1 unidad = "<<escala/distancia<<" metros"<<endl;
	cv::destroyWindow("Escala");
	return escala/distancia;
}


