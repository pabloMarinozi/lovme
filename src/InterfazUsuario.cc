#include "InterfazUsuario.h"
#include "Math.h"
#include <CreadorPunto3D.h>
#include <climits>
#include <cfloat>
#if WIN32
  #include <windows.h>
#else
  #include <X11/Xlib.h>
#endif


bool primerPuntoAceptado = false;
 bool eventLeftClick = false;
 bool eventRightClick = false;
 bool eventDoubleRightClick = false;
 bool eventoClickIzquierdo = false;
 bool eventoClickDerecho = false;
 bool eventoClickMedio = false;
 cv::Point pointSelected = Point(-1,-1);
 vector<long unsigned int> kfIds;
 int idPunto=0;
 int cantHP=0;
 int nHP=0;
 int hot=0;
 int videoId = 0;
 string str;
 void mostrarKeyframes(ORB_SLAM2::System* Sistema);
 long unsigned int ElegirKeyFrame(vector<ORB_SLAM2::KeyFrame*> excluidos, ORB_SLAM2::System* Sistema);
 vector<cv::Point> ElegirPuntos(int cant, long unsigned int kf,ORB_SLAM2::System* Sistema,cv::Mat &im);

//...

void getScreenResolution(int &width, int &height) {
#if WIN32
    width  = (int) GetSystemMetrics(SM_CXSCREEN);
    height = (int) GetSystemMetrics(SM_CYSCREEN);
#else
    Display* disp = XOpenDisplay(NULL);
    Screen*  scrn = DefaultScreenOfDisplay(disp);
    width  = scrn->width;
    height = scrn->height;
#endif
}
//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
bool preguntaSiNo(string preguntaInicial, string mensajeError){
	int respuesta;
	cout<<preguntaInicial<<endl;
	cout << "1: Sí."<<endl<<
			"2: No."<<endl;
	cin >> respuesta;
	while (cin.fail() ||respuesta>2 || respuesta <1) {
		cin.clear(); // clear input buffer to restore cin to a usable state
		cin.ignore(INT_MAX, '\n'); // ignore last input
		cout << mensajeError<<endl;
		cout<<preguntaInicial<<endl;
		cout << "1: Sí."<<endl<<
				"2: No."<<endl;
		cin >> respuesta;
	}
	return respuesta == 1;
}
//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
int preguntaOpciones(string preguntaInicial, std::vector<string> opciones){
	int respuesta;
	cout<<preguntaInicial<<endl;
	for(int i = 0; i<opciones.size();i++){
		cout<<i+1<<": "<<opciones[i]<<endl;
	}
	cin >> respuesta;
	while (cin.fail() ||respuesta>opciones.size() || respuesta <1) {
		cin.clear(); // clear input buffer to restore cin to a usable state
		cin.ignore(INT_MAX, '\n'); // ignore last input
		cout << "SOLO PUEDE ELEGIR UN NÚMERO QUE APAREZCA ENTRE LAS OPCIONES!!"<<endl;
		cout<<preguntaInicial<<endl;
		for(int i = 0; i<opciones.size();i++){
			cout<<i+1<<": "<<opciones[i]<<endl;
		}
		cin >> respuesta;
	}
	return respuesta;
}
//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
int solicitaEntero(string pregunta, int min, int max){
	int respuesta;
	cout<<pregunta<<endl;
	cin >> respuesta;
	while (cin.fail() ||respuesta>max || respuesta <min) {
		cin.clear(); // clear input buffer to restore cin to a usable state
		cin.ignore(INT_MAX, '\n'); // ignore last input
		cout << "SOLO PUEDE ELEGIR UN NÚMERO ENTERO ENTRE "<<min<<" Y "<<max<<endl;
		cout<<pregunta<<endl;
		cin >> respuesta;
	}
	return respuesta;
}
//--------------------------------------------------------------------------------------------------------------------------------------------------------------------------
float solicitaReal(string pregunta, float min, float max){
	float respuesta;
	cout<<pregunta<<endl;
	cin >> respuesta;
	while (cin.fail() ||respuesta>max || respuesta <min) {
		cin.clear(); // clear input buffer to restore cin to a usable state
		cin.ignore(INT_MAX, '\n'); // ignore last input
		cout << "SOLO PUEDE ELEGIR UN NÚMERO REAL ENTRE "<<min<<" Y "<<max<<endl;
		cout<<pregunta<<endl;
		cin >> respuesta;
	}
	return respuesta;
}
//--------------------------------------------------------------------------------------------------------------------------------------------------------------------------
double solicitaRealDoblePrecision(string pregunta, double min, double max){
	double respuesta;
	cout<<pregunta<<endl;
	cin >> respuesta;
	while (cin.fail() ||respuesta>max || respuesta <min) {
		cin.clear(); // clear input buffer to restore cin to a usable state
		cin.ignore(INT_MAX, '\n'); // ignore last input
		cout << "SOLO PUEDE ELEGIR UN NÚMERO REAL ENTRE "<<min<<" Y "<<max<<endl;
		cout<<pregunta<<endl;
		cin >> respuesta;
	}
	return respuesta;
}
 //-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 void onMouse(int evt, int x, int y, int flags, void* param) {
	if(evt==0 && primerPuntoAceptado){
		 cv::Point* ptPtr = (cv::Point*)param;
		 ptPtr->x = x;
		 ptPtr->y = y;
	 }
	 else if(evt == 1) {
		 eventoClickIzquierdo = true;
		 cv::Point* ptPtr = (cv::Point*)param;
		 ptPtr->x = x;
		 ptPtr->y = y;
	 }else if(evt == 2){
		 eventoClickDerecho = true;
	 }else if(evt == 7){
		 eventoClickMedio = true;
 	}
 }
//--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
void onMouse2(int evt, int x, int y, int flags, void* param) {
	if(evt == 1) {
		eventLeftClick = true;
	}else if(evt == 2){
		eventRightClick = true;
	}else if(evt == 8){
		eventDoubleRightClick = true;
	}
}

//--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
long unsigned int ElegirKeyFrame(vector<ORB_SLAM2::KeyFrame*> excluidos, ORB_SLAM2::System* Sistema){
	//obtengo la lista de KFs
	vector<ORB_SLAM2::KeyFrame*> allKFs = Sistema->mpMap->GetAllKeyFrames();
	vector<ORB_SLAM2::KeyFrame*>::iterator kf;
	long unsigned int kfId;

	cout << "\nElija un KeyFrame de entre los siguientes para marcar un hotpoint\n";
	for (kf = allKFs.begin(); kf != allKFs.end(); ++kf) {
		if (!BuscarKF((*kf)->mnId, excluidos)) {
			cout << (*kf)->mnId << ", ";
		}
	}
	cout<<endl;
	cout<<"... o ingrese + para ver un pasaje de todos los keyframes"<<endl;
	cin >> kfId;

	if(cin.fail()) mostrarKeyframes(Sistema);

	while (cin.fail() || !BuscarKF(kfId, allKFs) || BuscarKF(kfId, excluidos)) {
		cin.clear(); // clear input buffer to restore cin to a usable state
		cin.ignore(INT_MAX, '\n'); // ignore last input
		cout<< "\nSOLO PUEDE ELEGIR UN NÚMERO DE ENTRE LOS OFRECIDOS\n";
		sleep(1);
		cout<<"Elija un KeyFrame de entre los siguientes para marcar un hotpoint\n";
		for (kf = allKFs.begin(); kf != allKFs.end(); ++kf) {
			cout << (*kf)->mnId << ", ";
		}
		cout<<endl;
		cin >> kfId;
	}
	cout << "\nHas elegido el KeyFrame número "<<kfId<<endl;

	return kfId;
}
//--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
vector<cv::Point> ElegirPuntos(int cant, long unsigned int kf,ORB_SLAM2::System* Sistema, cv::Mat &im){
	//Inicializo puntos para el listener del mouse
	vector<cv::Point> puntos;
	cv::Point pointSelected, lastSelected;
	pointSelected.x = lastSelected.x = -1;
	pointSelected.y = lastSelected.x = -1;

	//recupero la imagen y la muestro por pantalla
	vector<ORB_SLAM2::KeyFrame*> allKFs = Sistema->mpMap->GetAllKeyFrames();
	cv::Mat img = BuscarKF(kf, allKFs)->getImGray();
	cv::imshow("ORB-SLAM2: KeyFrame Selected", img);
	img = Sistema->mpMap->ReproyectarTodo(BuscarKF(kf, allKFs),img);
	cout<<"\n\nNECESITAMOS QUE MARQUE "<<cant<<" PUNTOS EN LA SIGUIENTE IMAGEN\n"
			"(Si esta no es la imagen adecuada presione el click derecho sobre la imagen para elegir otra)\n";
	cv::namedWindow("ORB-SLAM2: KeyFrame Selected",cv::WINDOW_AUTOSIZE);
	cv::setMouseCallback("ORB-SLAM2: KeyFrame Selected", onMouse,
			(void*) &pointSelected);//establezco un listener de mouse sobre el frame
	cv::imshow("ORB-SLAM2: KeyFrame Selected", img);//muestro el frame por pantalla
	//cv::waitKey(100);


	//Capturo los puntos ingresados
	for (int i = 0; i < cant; ++i) {
		cout<<"\nHaga click donde se encuentre el punto n° "<<i+1<<endl;
		for (;;) {
			if (pointSelected.x != lastSelected.x && pointSelected.y != lastSelected.y) {
				cout << "Nuevo punto seleccionado: ("<<pointSelected.x<<","<<pointSelected.y<<")"<< endl;
				cv::Mat img2;
				drawPoint(img,pointSelected,img2,cv::Scalar(255,0,0,255),5);
				cv::imshow("ORB-SLAM2: KeyFrame Selected", img2);
				cv::waitKey(200);
				cout << "Si está de acuerdo con el punto seleccionado, presione el botón derecho. Sino, vuelva a seleccionar otro punto"<< endl;
				lastSelected.x = pointSelected.x;
				lastSelected.y = pointSelected.y;
			}
			if (eventoClickDerecho) {//si presiona click derecho se guarda el punto seleccionado
				if (pointSelected.x == -1) {
					cout<<"\nSE HA CANCELADO LA SELECCIÓN DE PUNTOS SOBRE ESTA IMAGEN"<<endl;
					eventoClickDerecho = false;
					cv::destroyWindow("ORB-SLAM2: KeyFrame Selected");
					return puntos;
					break;
				}else{
					cout<<"\nEl punto ingresado ha sido confirmado y se marcará en blanco"<<endl;
					drawPoint(img,pointSelected,img,cv::Scalar(255,255,255,255),5);
					cv::imshow("ORB-SLAM2: KeyFrame Selected", img);
					eventoClickDerecho = false;
					puntos.push_back(pointSelected);
					pointSelected.x = lastSelected.x = -1;
					pointSelected.y = lastSelected.x = -1;
					break;
				}
			}
			if (eventoClickMedio){
				cout<<"\nSE HA CANCELADO LA SELECCIÓN DE PUNTOS SOBRE ESTA IMAGEN"<<endl;
				cv::destroyWindow("ORB-SLAM2: KeyFrame Selected");
				eventoClickMedio = false;
				cv::destroyWindow("ORB-SLAM2: KeyFrame Selected");
				return puntos;
				break;
			}
			sleep(1);
		}
	}
	im = img;
	cv::destroyWindow("ORB-SLAM2: KeyFrame Selected");
	return puntos;
}

//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
void mostrarKeyframes(ORB_SLAM2::System* Sistema){
	vector<Mat> keyframes;
	vector<ORB_SLAM2::KeyFrame*> allKFs = Sistema->mpMap->GetAllKeyFrames();
	vector<ORB_SLAM2::KeyFrame*> someKFs;
	vector<ORB_SLAM2::KeyFrame*>::iterator kf;
	int min = solicitaEntero("Ingrese el primer keyframe del rango",allKFs.front()->mnId, allKFs.back()->mnId);
	int max = solicitaEntero("Ingrese el ultimo keyframe del rango",min, allKFs.back()->mnId);
	for (kf = allKFs.begin(); kf != allKFs.end(); ++kf) {
		if((*kf)->mnId<max && (*kf)->mnId>min)
			someKFs.push_back(*kf);
	}
	for (kf = someKFs.begin(); kf != someKFs.end(); ++kf) {
		Mat img = (*kf)->getImGray();
		stringstream ss;
		ss << (*kf)->mnId;
		Point center(img.cols/2,img.rows/2);
		putText(img, ss.str(), center, 1, 5, Scalar(255,255,255), 2,0);
		//img = Sistema->mpMap->ReproyectarPuntos((*kf),img);
		keyframes.push_back(img);
	}
	bool aceptado = false;
	if (keyframes.size()>0) {
		//SE MUESTRAN POR PANTALLA LOS KEYFRAMES
		cv::namedWindow("Keyframes");
		cv::setMouseCallback("Keyframes", onMouse2, (void*) &pointSelected);
		int i = 0;
		while(!aceptado){
			cv::Mat img = keyframes[i];
			cv::imshow("Keyframes",img);
			if(eventLeftClick) {
				aceptado = true;
				eventLeftClick = false;
			}
			if(i<keyframes.size()-1) i++;
			else i=0;
			sleep(1);
		}
		cv::destroyWindow("Keyframes");
	}else{
		cout<<"\n NO HAY KEYFRAMES EN ESTE RANGO \n";
	}
}
//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------





