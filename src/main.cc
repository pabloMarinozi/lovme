/**
* This file is part of ORB-SLAM2.
*
* Copyright (C) 2014-2016 Raúl Mur-Artal <raulmur at unizar dot es> (University of Zaragoza)
* For more information see <https://github.com/raulmur/ORB_SLAM2>
*
* ORB-SLAM2 is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* ORB-SLAM2 is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with ORB-SLAM2. If not, see <http://www.gnu.org/licenses/>.
*/

//inclusiones c++
#include <CreadorPunto3D.h>
#include<iostream>
#include<algorithm>
#include<fstream>
#include<chrono>
#include <termios.h>
#include <unistd.h>
#include <fcntl.h>
#include <InterfazUsuario.h>
#include <stdio.h>
#include <thread>
#include <mutex>
#include <sstream>
#include <regex>

//inclusiones opencv
#include<opencv2/core/core.hpp>
#include "opencv2/imgcodecs.hpp"
#include "opencv2/imgproc.hpp"
#include <opencv2/highgui.hpp>
#include <opencv2/videoio.hpp>

//inclusiones ORB_SLAM original
#include "System.h"
#include "Viewer.h"
#include "FrameDrawer.h"
#include "LocalMapping.h"
#include "Tracking.h"

//inclusiones clases propias
#include "Serializer.h"
#include "EtiquetadoTesteo.h"
#include "ProgramacionEstructuras.h"

//inclusiones serializacion clases polimorficas
#include <boost/archive/binary_oarchive.hpp>
#include <boost/archive/binary_iarchive.hpp>
#include <boost/serialization/export.hpp>
BOOST_CLASS_EXPORT(Punto)

using namespace std;
ORB_SLAM2::System *Sistema2, *Sistema;
void cargarMapa(string mapFile);
void LoadImages(const string &strFile, vector<string> &vstrImageFilenames,
		vector<double> &vTimestamps);
inline bool exists (const std::string& name);

int main(int argc, char **argv)
{
	if(argc != 6 && argc != 7)
	{
		cerr << endl << "Usage: ./mono_tum path_to_vocabulary path_to_settings path_to_sequence map_name_file video_id [observations_file]" << endl;
		return 1;
	}

	//OBTIENE LA RUTA A LAS IMÁGENES DEL ARCHIVO rgb.txt
	vector<string> vstrImageFilenames;
	vector<double> vTimestamps, vTimeHp;
	string strFile = string(argv[3])+"/rgb.txt";
	LoadImages(strFile, vstrImageFilenames, vTimestamps);
	string mapFile = argv[4];
	videoId = atoi(argv[5]);
	bool hayWebcam = false;
	bool cargadoMapa = false;
	int nImages = vstrImageFilenames.size();

	// Vector for tracking time statistics
	vector<float> vTimesTrack;
	vTimesTrack.resize(nImages);
	int opcion = -1;


	// CREA EL SISTEMA SLAM, INICIALIZA TODOS SUS HILOS Y LO ALISTA PARA PROCESAR FRAMES
	ORB_SLAM2::System SLAM(argv[1],argv[2],ORB_SLAM2::System::MONOCULAR,true);
	Sistema = Sistema2 =&SLAM;
	SLAM.serializer->setNombreMapa(mapFile);


	//SE ELIGE EL MODO DE EJECUCION DEL PROGRAMA
	vector<string> opciones = { "Generar Mapa a partir de un video",
			"Ejecutar solo localización con un mapa prexistente",
			"Crear nuevas estructuras", "Eliminar una estructura del mapa",
			"Editar estructuras del mapa", "Etiquetar Testeo",
			"Asignar escala al Mapa", };
	opcion = preguntaOpciones("\n\n\n\n\n\nIngrese el número correspondiente a la opción que desea ejecutar\n",opciones);
	cout << "has elegido la opcion " << opcion << endl;

	//////////////////////////////////////////////BUCLE DE EJECUCION PRINCIPAL//////////////////////////////////////////////
	if (opcion<3) {
		cout << endl << "-------" << endl;
		cout << "Start processing sequence ..." << endl;
		cout << "Images in the sequence: " << nImages << endl << endl;
	}
	cv::Mat im;
	mutex m;
	vector<int> hpFrames;

	for (;;) {
		for(int ni=0; ni<nImages; ni++)
		{
			// LEE LA IMAGEN
			if(opcion > 2)
				im = Mat(480,640,CV_8U,255); //pone una imagen blanca
			else
				im = cv::imread(string(argv[3])+"/"+vstrImageFilenames[ni],-1);
			double tframe = vTimestamps[ni];

			if(im.empty()){
				cerr << endl << "Failed to load image at: "
						<< string(argv[3]) << "/" << vstrImageFilenames[ni] << endl;
				return 1;}
			std::chrono::steady_clock::time_point t1 = std::chrono::steady_clock::now();

			Sistema->mpViewer->Release();
			//LE DA LA IMAGEN AL SISTEMA ORB_SLAM
			SLAM.TrackMonocular(im,tframe);

			//CARGA EL MAPA SI ES NECESARIO
			if(opcion > 1 && !cargadoMapa){
				cargarMapa(mapFile);
				cargadoMapa = true;
				vector<Estructura*> estructuras = Sistema->mpMap->GetAllEstructuras();

				//SI SE VAN A PROGRAMAR PUNTOS SOBRE EL MAPA
				if (opcion == 3) {
					cout<<"\n ACTUALMENTE HAY "<<estructuras.size()<<" PUNTOS EN ESTE MAPA!!\n"<<endl;

					Sistema->mpViewer->Release();
					//se ingresa por pantalla la cantidad de puntos a programar
					cantHP = solicitaEntero("\n\n¿Cuantas estructuras desea programar?\n", 0);

					//BUCLE DE CREACION DE ESTRUCTURAS
					while(cantHP>0 && SLAM.mpTracker->mState == ORB_SLAM2::Tracking::LOST){
						ExpertoEstructuras* experto = new ExpertoEstructuras();
						bool exito = experto->ProgramarEstructura();
						if (exito) {
							cantHP--;
							nHP++;
						}
						cout<<"Hasta ahora se han creado "<<nHP<<" estructuras."<<endl;
					}

					//se guarda el mapas con los nuevos estructuras
					std::string s(Sistema->serializer->getNombreMapa());
					std::regex e("-con[0-9]+estructuras");
					estructuras = Sistema->mpMap->GetAllEstructuras();
					std::string file;
					if (std::regex_match (s,e)){
						file = std::regex_replace (s,e,"-con"+to_string(estructuras.size())+"estructuras");
					}else
						file = s+"-con"+to_string(estructuras.size())+"estructuras";
					Sistema->serializer->mapSave(file);
					return 0;
				}


				//SE VAN A BORRAR O EDITAR PUNTOS
				if(opcion == 4 || opcion == 5){
					bool seguir = true;
					string str;
					int seguro=0;
					ExpertoEstructuras* exp = new ExpertoEstructuras();
					do {
						if(opcion == 4){
							exp->EliminarEstructura(mapFile, Sistema);
							str = "eliminar";
						}
						else{
							exp->EditarEstructura(mapFile,Sistema);
							str = "editar";
						}
						seguro = preguntaSiNo("¿Desea "+str+" otra estructura?","SOLO PUEDE CONTESTAR SÍ O NO");
						if(seguro==2)
							seguir = false;

					} while (seguir==true);
					exit(0);
				}

				//Se quiere marcar en un video de testeo la ubicación real de los puntos programados
				if(opcion == 6){
					//se setea la ruta al archivo de observaciones
					string rutaObservaciones;
					if(argc != 7){
						cout << "Ingrese la ruta al archivo de observaciones."<<endl;
						getline(cin,rutaObservaciones);
					}else
						rutaObservaciones = argv[6];
					while(!exists(rutaObservaciones)){
						cout << "\n\nLA RUTA INGRESADA FUE '"<<rutaObservaciones<<"', PERO NO EXISTE NINGÚN ARCHIVO EN ESA UBICACIÓN."<<endl;
						cout << "Ingrese la ruta al archivo de observaciones."<<endl;
						getline(cin,rutaObservaciones);
					}

					//se inicia con el etiquetado
					ExpertoEtiquetar* experto = new ExpertoEtiquetar(rutaObservaciones,vstrImageFilenames,vTimestamps,string(argv[3]));
					experto->EtiquetarTesteo();
					exit(0);
				}


				if(opcion == 7){
					if(Sistema->mpMap->getEscala() != 0){
						char pregunta[1000];
						sprintf(pregunta, "Este mapa ya tiene la siguiente relación de escala: \n"
								"\n1 unidad = %f metros.\n"
								"Desea modificar la relación de escala?\n",Sistema->mpMap->getEscala());
						bool respuesta = preguntaSiNo(pregunta, "\n\nSOLO PUEDES INGRESAR NUMEROS QUE CORRESPONDAN CON ALGUNA OPCION.\n");
						if(!respuesta) exit(0);
					}
					ExpertoEstructuras* exp = new ExpertoEstructuras();
					double escala = exp->CalcularEscala();
					Sistema->mpMap->setEscala(escala);
					cout<<"Se guardará el mapa con la nueva escala"<<endl;
					std::string s(Sistema->serializer->getNombreMapa());
					std::regex e("-conEscala");
					std::string file;
					if (std::regex_match (s,e)){
						file = s;
					}else
						file = s+"-conEscala";
					Sistema->serializer->mapSave(file);
					return 0;
				}
			}

			//ESPERA EL TIEMPO NECESARIO PARA CARGAR EL SIGUIENTE FRAME DE MODO QUE PAREZCA QUE EL VIDEO SE EJECUTA EN TIEMPO REAL
			std::chrono::steady_clock::time_point t2 = std::chrono::steady_clock::now();
			double ttrack= std::chrono::duration_cast<std::chrono::duration<double> >(t2 - t1).count();
			vTimesTrack[ni]=ttrack;
			if (opcion!=3) {
				double T=0;
				if(ni<nImages-1)
					T = vTimestamps[ni+1]-tframe;
				else if(ni>0)
					T = tframe-vTimestamps[ni-1];

				if(ttrack<T)
					usleep((T-ttrack)*1e6);
			}
		}
		if(!hayWebcam){
			//////////////////////////////////////////////FIN BUCLE DE EJECUCION PRINCIPAL//////////////////////////////////////////////

			//CALCULA ESTADÍSTICAS DE DESEMPEÑO DE ORB_SLAM
			SLAM.serializer->mapSave(SLAM.serializer->getNombreMapa());
			cout << "Se acabaron las imágenes para procesar."<<endl;
			SLAM.guardarObservaciones("observaciones.txt");
			// Stop all threads
			SLAM.Shutdown();
			// Tracking time statistics
			sort(vTimesTrack.begin(),vTimesTrack.end());
			float totaltime = 0;
			for(int ni=0; ni<nImages; ni++)
				totaltime+=vTimesTrack[ni];
			cout << "-------" << endl << endl;
			cout << "median tracking time: " << vTimesTrack[nImages/2] << endl;
			cout << "mean tracking time: " << totaltime/nImages << endl;
			// Save camera trajectory
			SLAM.SaveKeyFrameTrajectoryTUM("KeyFrameTrajectory.txt");


			return 0;
		}
	}
}


//--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
void LoadImages(const string &strFile, vector<string> &vstrImageFilenames, vector<double> &vTimestamps)
{
    ifstream f;
    f.open(strFile.c_str());

    // skip first three lines
    string s0;
    getline(f,s0);
    getline(f,s0);
    getline(f,s0);

    while(!f.eof())
    {
        string s;
        getline(f,s);
        if(!s.empty())
        {
            stringstream ss;
            ss << s;
            double t;
            string sRGB;
            ss >> t;
            vTimestamps.push_back(t);
            ss >> sRGB;
            vstrImageFilenames.push_back(sRGB);
        }
    }
}
//--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
void cargarMapa(string mapFile){
	// El reset subsiguiente requiere que LocalMapping no esté pausado.
	Sistema->mpLocalMapper->Release();

	// Limpia el mapa de todos los singletons
	Sistema->mpTracker->Reset();
	// En este punto el sistema está reseteado.

	// Espera a que se detenga LocalMapping y  Viewer
	Sistema->mpLocalMapper->RequestStop();
	Sistema->mpViewer	  ->RequestStop();

	cout << "Abriendo archivo " << mapFile << endl;

	Sistema->serializer->mapLoad(mapFile);

	//genera los loopedges porque serializarlos generaba un error de boost que no supe resolver
	vector<ORB_SLAM2::KeyFrame*> edges;
	vector<ORB_SLAM2::KeyFrame*> allKFs = Sistema->mpMap->GetAllKeyFrames();
	vector<ORB_SLAM2::KeyFrame*>::iterator kf;
	for (kf = allKFs.begin(); kf != allKFs.end(); ++kf) {
		if ((*kf)->mbNotErase) {
			edges.push_back((*kf));
			cout<<"Generando loopedge id: "<<(*kf)->mnId<<endl;
		}
	}
	if(edges.size()==2){
		edges[0]->AddLoopEdge(edges[1]);
		edges[1]->AddLoopEdge(edges[0]);
	}
	cout << "Mapa cargado." << endl;

	Sistema->mpTracker->mState = ORB_SLAM2::Tracking::LOST;

	// Reactiva viewer.  No reactiva el mapeador, pues el sistema queda en sólo tracking luego de cargar.
	Sistema->mpViewer->Release();
	Sistema->ActivateLocalizationMode();


	// Por las dudas, es lo que hace Tracking luego que el estado pase a LOST.
	// Como tiene un mutex, por las dudas lo invoco después de viewer.release.
	Sistema->mpFrameDrawer->Update(Sistema->mpTracker);
}

// Driver function to sort the vector elements
// by second element of pairs
bool sortbysec(const tuple<double, int, double, double> &a,
              const tuple<double, int, double, double> &b)
{
    if(get<1>(a) == get<1>(b))
    	return(get<0>(a) < get<0>(b));
    else
    	return (get<1>(a) < get<1>(b));
}

inline bool exists (const std::string& name) {
    if (FILE *file = fopen(name.c_str(), "r")) {
        fclose(file);
        return true;
    } else {
        return false;
    }
}


