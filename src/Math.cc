/*
 * Mat.cc
 *
 *  Created on: 5 feb. 2019
 *      Author: pablo
 */
#include "Math.h"
#include<iostream>

double distanciaPuntoPunto(cv::Point2d punto1, cv::Point2d punto2){
	return sqrt( pow(punto1.x-punto2.x,2) +
				 pow(punto1.y-punto2.y,2));
}
double distanciaPuntoSegmento(cv::Point2d punto, cv::Point2d s1, cv::Point2d s2){
	//encontramos el t que minimiza la distancia entre la recta s(t)= s1+t*(s2-s1) y el punto
	double num_t = (punto-s1).dot(s2-s1);
	double den_t = pow(distanciaPuntoPunto(s1,s2),2);
	double min_t = num_t/den_t;

	//cualquier punto s(t_0) con t_0 perteneciente a [0,1] está dentro del segmento
	//si t_0 < 0, entonces s(0) es el punto más cercano, sino (t_0 > 0) lo es s(1)
	double t = min( max(min_t,0.0),1.0 );

	//ahora encontramos s(t) que es el punto del segmento más cercano al punto
	cv::Point2d s_t = s1 + t*(s2-s1);
	double resul = distanciaPuntoPunto(punto,s_t);
	return resul;
}

double distanciaPuntoPoligono(cv::Point2d p, vector<cv::Point2d> polygon){
	int n = polygon.size();;
	// There must be at least 3 vertices in polygon[]
	if (n < 3)  return -1;

	// Itera por los lados para encontrar el más cercano al punto
	double min = INF;
	for(int i = 0; i<n;i++){
		double dis;
		if(i!=n-1)
			dis = distanciaPuntoSegmento(p,polygon[i],polygon[i+1]);
		else
			dis = distanciaPuntoSegmento(p,polygon[i],polygon[0]);
		if(dis < min) min=dis;
	}

	// Devuelve la distancia del punto al lado más cercano
	    return min;
}
//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

// Given three colinear cv::Point2ds p, q, r, the function checks if
// cv::Point2d q lies on line segment 'pr'
bool onSegment(cv::Point2d p, cv::Point2d q, cv::Point2d r)
{
    if (q.x <= max(p.x, r.x) && q.x >= min(p.x, r.x) &&
            q.y <= max(p.y, r.y) && q.y >= min(p.y, r.y))
        return true;
    return false;
}

// To find orientation of ordered triplet (p, q, r).
// The function returns following values
// 0 --> p, q and r are colinear
// 1 --> Clockwise
// 2 --> Counterclockwise
int orientation(cv::Point2d p, cv::Point2d q, cv::Point2d r)
{
    int val = (q.y - p.y) * (r.x - q.x) -
              (q.x - p.x) * (r.y - q.y);

    if (val == 0) return 0;  // colinear
    return (val > 0)? 1: 2; // clock or counterclock wise
}

// The function that returns true if line segment 'p1q1'
// and 'p2q2' intersect.
bool doIntersect(cv::Point2d p1, cv::Point2d q1, cv::Point2d p2, cv::Point2d q2)
{
    // Find the four orientations needed for general and
    // special cases
    int o1 = orientation(p1, q1, p2);
    int o2 = orientation(p1, q1, q2);
    int o3 = orientation(p2, q2, p1);
    int o4 = orientation(p2, q2, q1);

    // General case
    if (o1 != o2 && o3 != o4)
        return true;

    // Special Cases
    // p1, q1 and p2 are colinear and p2 lies on segment p1q1
    if (o1 == 0 && onSegment(p1, p2, q1)) return true;

    // p1, q1 and p2 are colinear and q2 lies on segment p1q1
    if (o2 == 0 && onSegment(p1, q2, q1)) return true;

    // p2, q2 and p1 are colinear and p1 lies on segment p2q2
    if (o3 == 0 && onSegment(p2, p1, q2)) return true;

     // p2, q2 and q1 are colinear and q1 lies on segment p2q2
    if (o4 == 0 && onSegment(p2, q1, q2)) return true;

    return false; // Doesn't fall in any of the above cases
}
bool isInside(cv::Point2d p, vector<cv::Point2d> polygon)
{
	int n = polygon.size();;



	// There must be at least 3 vertices in polygon[]
    if (n < 3)  return false;

    // Create a cv::Point2d for line segment from p to infinite
    cv::Point2d extreme = {INF, p.y};

    // Count intersections of the above line with sides of polygon
    int count = 0, i = 0;
    do
    {
        int next = (i+1)%n;

        // Check if the line segment from 'p' to 'extreme' intersects
        // with the line segment from 'polygon[i]' to 'polygon[next]'
        if (doIntersect(polygon[i], polygon[next], p, extreme))
        {
            // If the cv::Point2d 'p' is colinear with line segment 'i-next',
            // then check if it lies on segment. If it lies, return true,
            // otherwise false
            if (orientation(polygon[i], p, polygon[next]) == 0)
               return onSegment(polygon[i], p, polygon[next]);

            count++;
        }
        i = next;
    } while (i != 0);

    // Return true if count is odd, false otherwise
    return count&1;  // Same as (count%2 == 1)
}



